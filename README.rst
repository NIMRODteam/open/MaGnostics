MaGnostics
==========

Code, mostly in python, for synthetic diaGnostics of Magnetic fusion
experiments.  

DiaGnostics originally comes from the roots Dia (way) and Gnosis
(knowledge).  This project is to increase knowledge of magnetic fusion
experiments through simulation.

These were developed as part of the ValTools project (S. Vadlamani and S.
Kruger) and the nimnostics project (J. Hebert).  Some warts associated 
with this history likely remain.

# Developing synthetic diagnostics

Developing and maintaining a set of synethetic diagnostics can be painful. 
The general issue is one of separation of the data needed:

  * Diagnostic information that is:
      - Generic:  This is how the diagnostic works (e.g., line integration)
      - Machine specific: no changes on a yearly time scale
      - Discharge specific: changes from either shot-to-shot or weekly/monthly
  * Code information:
      - Want to be able to swap codes in and out easily

Many synthetic diagnostics have been written in fortran with everything
hard-coded into the file.  This becomes a maintenance nightmare.

Using python, it is easy to separte out these concerns because of its
flexibility and superior I/O capabilities.  

Code organization
=================

Based on the above description, the main directories are:

  + code
     - How to access a given codes data
  + machine
     - Information related to a specific machine (e.g., geometry, filters, ...)
       in data files, as well as python scripts to read those datafiles
  + diagnostics
     - Diagnostic information organized by type.  Perform the actual 
       synthetic diagnostic calculation using data from the code and machine
  + utils
     - Common utility functions
  + examples
     - Example scripts that bring everything together.  
       Scripts used for figures used in publications for example should be placed in here.
