# -*- coding: utf-8 -*-

import sys
if sys.version_info[0] < 3:
    sys.exit('Sorry, Python < 3 is not supported')

__author__ = """NIMROD Team and contributors"""
__email__ = 'scott.e.kruger@gmail.com'
__version__ = '0.1.0'

__all__=['code','diagnostics','machine','utils']

from . import diagnostics
from . import machine
from . import code

# Don't do this
#from .diagnostics import *
