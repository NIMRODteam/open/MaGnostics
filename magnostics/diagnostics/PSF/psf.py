#!/usr/bin/env python
"""
  Classes and functions for using point spread functions (psf)

  Only single PSF.  Plotting kept separate to minimize dependencies
"""
import numpy as np
import scipy.interpolate as scinterp
import scipy.io as scio
import h5py
import os
import yaml


class singlePSF:
    """
    Class for a single PSF
    """

    def __init__(self, psfFileName=None, phi=0.0):
        """
        PSF functions are given on (R,Z) grids and are imagined to be
        part of a larger collection (channels) that are indexed.
        Multiple channel arrays are possible, so a label is included to
        self-identify what channel array this PSF is a member of.

        Also, PSF functions are calculated calculated onto a phi grid, but this
        location is generally not given in the PSF files.  See this in the
        constructor.

        Source of PSF data:
          o Unique file for this location
          o Single file for whole channel array
          o Analytic function
        """
        self.phi = 0.0
        self.values = []
        self.grid = []
        self.gridUnits = "m"  # Assume m for convenience
        self.centerLoc = []
        self.channelNumber = 1
        self.channelArrayLabel = ""
        self.file = None
        if psfFileName:
            self.psfFileName = psfFileName
            self.read(psfFileName)
        else:
            self.psfFileName = None
            self._setPsfFromFunction()

    def read(self, psfFileName):
        """
        Set field from a psf file. Added method.

        Assume PSF file either in IDL *.sav format
        """
        # file with psf data
        base, ext = os.path.splitext(psfFileName)
        if ext == ".sav":
            self._setPsfFromSavFile(psfFileName)
        elif ext in [".yml", ".yaml"]:
            self._setPsfFromYamlFile(psfFileName)
        else:
            print("File extension not recognized for file", psfFileName)
            print("PSF not set")

        self.psfFileName = psfFileName

    def _setPsfFromFunction(self):
        """
        For standard co-current cases, the PSF is roughly ellipsoide
        """
        # psf R,Z grid
        rlen = 0.03
        zlen = 0.03
        rg = 1.7 + rlen * np.linspace(0, 1, 100)
        zg = zlen * (np.linspace(0, 1, 50) - 0.5)
        self.grid = np.meshgrid(rg, zg, indexing="ij")
        rr, zz = self.grid
        # Use ellipse to describe PSF function
        self.values = np.exp(
            -(
                (rr - rg.mean()) ** 2 / (rlen / 2.0) ** 2
                + (zz - zg.mean()) ** 2 / (zlen / 2.0) ** 2
            )
        )
        # Center of grid is also center of this function
        self.centerLoc = np.array([rg.mean(), zg.mean()])

    def _setPsfFromYamlFile(self, yamlFile):
        """
        Read a yaml file that parameterizes a function
        Mostly used for testing
        """
        with open(yamlFile) as filestr:
            psfConfig = yaml.load(filestr, Loader=yaml.Loader)
        # psf R,Z grid
        rcenter = np.double(psfConfig["rcenter"])
        zcenter = np.double(psfConfig["zcenter"])
        rlen = np.double(psfConfig["rlen"])
        zlen = np.double(psfConfig["zlen"])
        rwidth = np.double(psfConfig["rwidth"])
        zwidth = np.double(psfConfig["zwidth"])
        rg = rcenter + rlen * (np.linspace(0, 1, 100) - 0.5)
        zg = zcenter + zlen * (np.linspace(0, 1, 50) - 0.5)
        self.grid = np.meshgrid(rg, zg, indexing="ij")
        rr, zz = self.grid
        # Use ellipse to describe PSF function
        self.values = np.exp(
            -(
                (rr - rg.mean()) ** 2 / (rwidth) ** 2
                + (zz - zg.mean()) ** 2 / (zwidth) ** 2
            )
        )
        # Center of grid is also center of this function
        self.centerLoc = np.array([rg.mean(), zg.mean()])

    def _setPsfFromSavFile(self, savFile):
        """
        Read a sav file in format that McKee writes it out in
        """
        psffile = scio.readsav(savFile)

        # Sav files have PSF(Z,R), we prefer more standard f(R,Z)
        self.values = psffile.psf.psf[0].transpose()
        rg = psffile.psf.r[0] * 0.01
        zg = psffile.psf.p[0] * 0.01
        self.grid = np.meshgrid(rg, zg, indexing="ij")
        self.centerLoc = np.array(
            [psffile.psf.stats[0][0][0] * 0.01, 
             psffile.psf.stats[0][0][1] * 0.01]
        )

    def integrate(self, func):
        """
        Integral of input function over PSF grid using splines.
        """
        # get 1D vectors
        R1D = self.grid[0][:, 0]
        Z1D = self.grid[1][0, :]

        # interpolate splines
        splRZ = scinterp.RectBivariateSpline(R1D, Z1D, func)

        # return integral the spline function
        return splRZ.integral(R1D[0], R1D[-1], Z1D[0], Z1D[-1])

    def dump(self, h5handle=None, h5FileName="PSF.h5"):
        """
      Write a single PSF to file.
      If a file is already open (e.g., as part of an array, then use
      handle, otherwise use filename.
      """
        close = False
        if not h5handle:
            h5handle = h5py.File(h5FileName, mode="w")
            close = True

        hf = h5handle
        hf.attrs.create("title", "PSF data")
        hf.attrs.create("phi", self.phi)
        hf.attrs.create("channelNumber", self.channelNumber)
        hf.attrs.create("channelArrayLabel", self.channelArrayLabel)
        hf.attrs.create("file", str(self.file))
        hf.attrs.create("centerLoc", self.centerLoc)
        # TODO:  Check that VizSchema is right
        dataset = hf.create_dataset("Rgrid", data=self.grid[0], dtype="d")
        dataset = hf.create_dataset("Zgrid", data=self.grid[1], dtype="d")
        dataset = hf.create_dataset("PSF", data=self.values, dtype="d")
        dataset.attrs.create("vsType", "variableWithMesh")
        dataset.attrs.create("vsNumSpatialDims", 2)
        dataset.attrs.create("vsLabels", "Rgrid,Zgrid")
        if close:
            h5handle.close()


def restore(h5handle=None, h5FileName="PSF.h5"):
    """
  This is a copy of the PSF restore only with PSF -> PSFplot

  This is one of those things that normal inheritance or metaprogramming
  doesn't solve and some of the other tricks don't seem to work.
  """
    close = False
    if not h5handle:
        h5handle = h5py.File(h5FileName, mode="r")
        close = True
    hf = h5handle

    newpsf = singlePSF()

    newpsf.phi = hf.attrs.get("phi")
    newpsf.channelNumber = hf.attrs.get("channelNumber")
    newpsf.channelArrayLabel = hf.attrs.get("channelArrayLabel")
    newpsf.file = hf.attrs.get("file")
    newpsf.centerLoc = hf.attrs.get("centerLoc")
    # TODO:  Check that VizSchema is right
    rg = hf.get("Rgrid")[:, :]
    zg = hf.get("Zgrid")[:, :]
    newpsf.grid = [rg, zg]
    newpsf.values = hf.get("PSF")[:, :]

    if close:
        h5handle.close()
    return newpsf
