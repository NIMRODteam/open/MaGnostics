#!/bin/env python

import os
import sys
import glob
import collections.abc
import h5py
import numpy as np
import re
import tables
import rzpoints
import yaml


# This is to handle python3 changes to how import paths work
# This is to allow calling this file directly, otherwise I would
# use the import .PSF syntax
file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

import psf


# See:
# https://stackoverflow.com/questions/4836710/is-there-a-built-in-function-for-string-natural-sort
def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split("([0-9]+)", key)]
    return sorted(l, key=alphanum_key)


class psfChannelArrays:
    def __init__(self, configFile=None, restoreDict=None):
        """
        Create a structure with a collection of arrays
        """
        # Defaults
        self.hasPsfInfo = False
        self.checkArrayPSFconsistency = False

        configDict = None

        # Useful for testing and examples
        if not configFile and not restoreDict:
            print("Using example data")
            configDict = self._createExampleConfig()

        # Get the data which is in a yaml format
        if configFile:
            if os.path.exists(configFile):
                self.configFile = configFile
                with open(configFile) as filestr:
                    configDict = yaml.load(filestr, Loader=yaml.Loader)
            else:
                print("Error: " + configFile + "not found.")
                return

        # Initialize from configDict
        if configDict:
            #  Now given configDict process into our own datastruct
            if "arrays" in configDict:
                self.arrayNames = configDict["arrays"].split(",")
            else:
                raise Exception("ERROR: Must specify arrays in configuration.")
            allArrays = []
            allOthers = {}
            for key in configDict:
                if key == "arrays":
                    continue
                keyval = configDict[key]
                # Arrays are dictionaries and are needed
                if isinstance(keyval, collections.abc.Mapping):
                    for needed in "type nRChannels nZChannels".split():
                        if needed not in keyval:
                            msg = "Incorrect array initialization.  "
                            msg += needed + " not found in array: " + key
                            raise Exception(msg)
                    allArrays.append(key)
                    if key not in self.arrayNames:
                        continue
                    # Because the locations of the PSF centers should
                    # come from the PSF itself, we need dummy values if they
                    # aren't specified
                    for possible in "Ro Zo dR dZ".split():
                        if possible not in keyval:
                            keyval[possible] = 0.5
                    array = rzpoints.diagnosticPoints(keyval)
                    array.name = key
                    setattr(self, key, array)
                    if "psfFileList" in keyval:
                        a = getattr(self, key, array)
                        a.psfFileList = keyval["psfFileList"].split(",")
                        self.hasPsfInfo = True
                else:
                    # These are not strictly needed but may want
                    # to keep track of (shot number, etc.)
                    # Provenance and metadata are good things!
                    allOthers[key] = keyval
                allOthers["allArrays"] = allArrays
                self.info = allOthers

        # Initialize from restoreDict
        elif restoreDict:
            if "arrayNames" not in restoreDict:
                print("No arrays in restore file")
                return

            for aname in restoreDict["arrayNames"]:
                NR = restoreDict[aname]["NR"]
                NZ = restoreDict[aname]["NZ"]
                adict = {
                    "type": "array",
                    "nRChannels": NR,
                    "nZChannels": NZ,
                    "Ro": 1.0,
                    "Zo": 1.0,
                }
                array = rzpoints.diagnosticPoints(adict)
                array.RZpts = restoreDict[aname]["RZpts"]
                array.NR = NR
                array.NZ = NZ
                setattr(self, aname, array)

                if "PSFs" in restoreDict[aname]:
                    array.psfArray = np.empty((NR, NZ), dtype=object)
                    for p in restoreDict[aname]["PSFs"]:
                        sp = p.split("_")
                        i = int(sp[1])
                        j = int(sp[2])
                        array.psfArray[i, j] = restoreDict[aname]["PSFs"][p]

            # MetaData
            self.info = {}
            for key in restoreDict:
                if key == "arrayNames":
                    continue
                if key in restoreDict["arrayNames"]:
                    continue
                self.info[key] = restoreDict[key]

    def _createExampleConfig(self):
        """
        Create the same data a conf file would
        """
        confDict = {
            "machine": "FooTok",
            "diagnostic": "BES",
            "arrays": "array1,array2",
            "array1": {
                "type": "array",
                "nRChannels": 5,
                "nZChannels": 6,
                "dR": 0.009,
                "dZ": 0.012,
                "Ro": 1.7,
                "Zo": 0.0,
            },
            "array2": {
                "type": "array",
                "nRChannels": 1,
                "nZChannels": 10,
                "dZ": 0.012,
                "Ro": 1.7,
                "Zo": 0.0,
            },
        }
        return confDict

    def addPSF(self, psfFile=None, psfDir=None, recenterPSF=False,
               Rshift=0.0, Zshift=0.0):
        """
        Read a specific shotfile and add PSFfiles to each channel array
        If psfDir, then structure must be:
             psfDir/<arrayName>/<psfFile(s)>
        """
        if psfFile:
            if not os.path.isfile(psfFile):
                print("Error: File ", psfFile, " does not exist.")
                return

        if psfDir:
            if not os.path.isdir(psfDir):
                print("Error: Directory ", psfFile, " does not exist.")
                return

        if psfFile and psfDir:
            print("Cannot specify both a single file and a directory.")
            return

        if not psfFile and not psfDir:
            if not self.hasPsfInfo:
                print("Must specify both a single file and a directory.")
                return

        self.psfFile = psfFile
        self.psfDir = psfDir

        for arrayName in self.arrayNames:
            array = getattr(self, arrayName)
            self._putPSFonArray(array, recenterPSF, Rshift, Zshift)

        self.hasPsfInfo = True

    def _putPSFonArray(self, array, recenterPSF, Rshift, Zshift):
        """
        At each geometric center (RZpts), place a PSF field (PSF object).
        This adds a numpy array of PSF objects with the same dimensions
        as RZpts[0,:,:]

        There are two ways of specifying the PSF for the channel array:
            1. Use 1 psf file for entire array
            2. Specify a directory that contains a subdir containing
               the files:
                   psfDir/<arrayName>/psf<Number>.{sav,yml}

        When coming from multiple files, the key is to map the array
        geometry [(i,j) index] onto file name [<Number>].
        Because this cannot really be standardized, this step
        is not part of the constructor.
        It is suggested that when setting up a shot to debug this mapping.

        """
        debug = False
        Rshift = np.double(Rshift)
        Zshift = np.double(Zshift)

        # This is the datastructure that we are going to fill up
        NR = array.RZpts[0].shape[0]
        NZ = array.RZpts[1].shape[1]
        array.NR = NR
        array.NZ = NZ
        Nchannels = NR * NZ
        array.psfArray = np.empty((NR, NZ), dtype=object)
        arrayName = array.name
        psfFileList = [None] * Nchannels
        if hasattr(array, "psfFileList"):
            psfFileList = getattr(array, "psfFileList")
            if debug:
                print("psfFileList found: ", psfFileList)
            if self.psfFile or self.psfDir:
                print("Warning: overwriting psfFileList in file: ",
                      self.configFile)
            confFileDir = os.path.abspath(os.path.dirname(self.configFile))
            newList = []
            for f in psfFileList:
                newList.append(os.path.join(confFileDir, arrayName, f.strip()))
            psfFileList = newList

        if self.psfFile:
            psfFileList = [self.psfFile] * Nchannels

        if self.psfDir:
            savFiles = glob.glob(os.path.join(self.psfDir, arrayName, "*.sav"))
            ymlFiles = glob.glob(os.path.join(self.psfDir, arrayName, "*.yml"))
            if len(savFiles) > 0 and len(ymlFiles) > 0:
                print("Error: Cannot have both yaml & sav files in directory",
                      arrayName)
                return
            if len(savFiles) + len(ymlFiles) == 1:
                psfFileList = (savFiles + ymlFiles) * Nchannels
            else:
                self.checkArrayPSFconsistency = True
                psfFileList = natural_sort(savFiles + ymlFiles)
            if len(psfFileList) != Nchannels:
                raise Exception(
                    "Warning:  mismatch between psf file list and nchannels"
                )

        ifile = 0
        for j in range(NZ):
            for i in range(NR):
                array.psfArray[i, j] = psf.singlePSF(psfFileList[ifile])

                # Useful data for keeping track
                array.psfArray[i, j].channelNumber = ifile
                array.psfArray[i, j].file = psfFileList[ifile]
                array.psfArray[i, j].channelArrayLabel = arrayName
                ifile += 1

                if recenterPSF:
                    # Recenter the PSF grid to the center of the geom file
                    psfCen = array.psfArray[i, j].centerLoc
                    RZshift = array.RZpts[:, i, j] - psfCen[:]
                    array.psfArray[i, j].grid[0] += RZshift[0]
                    array.psfArray[i, j].grid[1] += RZshift[1]
                    array.psfArray[i, j].centerLoc = array.RZpts[:, i, j]

                # Shift the grid if requested
                array.psfArray[i, j].grid[0] += Rshift
                array.psfArray[i, j].grid[1] += Zshift
                array.psfArray[i, j].centerLoc += np.array([Rshift, Zshift])

        # Save the list
        array.psfFileList = psfFileList

        # The sav files contain the centers so we may need to redo the
        # rzpoints and possibly reorder the arrays
        if self.checkArrayPSFconsistency:
            self._makeArrayPSFconsistent(array)

    def _makeArrayPSFconsistent(self, array):
        """
        Make sure RZpts are consistent with the centers and that the centers
        are ordered in the grid correctly
        """
        NR = array.NR
        NZ = array.NZ
        centers = np.zeros((2, NR, NZ), np.double)
        for j in range(NZ):
            for i in range(NR):
                centers[:, i, j] = array.psfArray[i, j].centerLoc

        # These are what we are going to fill up in the sorting
        array.RZpts = np.zeros((2, NR, NZ), np.double)
        ijmap = np.zeros((NR, NZ), np.int)

        rremain = centers[0, :, :].reshape(1, NR * NZ)[0]
        zremain = centers[1, :, :].reshape(1, NR * NZ)[0]
        ijremain = np.arange(NR * NZ)
        for i in range(NR):
            # Get the first column:  NZ lowest R's and then sort
            rsrsort = rremain[rremain.argsort()]
            zsrsort = zremain[rremain.argsort()]
            ijrsort = ijremain[rremain.argsort()]

            rscol = rsrsort[0:NZ]
            zscol = zsrsort[0:NZ]
            ijcol = ijrsort[0:NZ]
            rremain = rsrsort[NZ:]
            zremain = zsrsort[NZ:]
            ijremain = ijrsort[NZ:]

            array.RZpts[0, i, 0:NZ] = rscol[zscol.argsort()]
            array.RZpts[1, i, 0:NZ] = zscol[zscol.argsort()]
            ijmap[i, 0:NZ] = ijcol[zscol.argsort()]

        # Reorder the psfArray now as well
        psfArray1D = array.psfArray.reshape(1, NR * NZ)[0]
        for j in range(NZ):
            for i in range(NR):
                array.psfArray[i, j] = psfArray1D[ijmap[i, j]]

    def _dumpAttributes(self, hfile, loc):
        """ Write the basic attributes to an h5file """
        hfile.set_node_attr(loc, "nChannels", self.nChannels)
        hfile.set_node_attr(loc, "dR", self.dR)
        hfile.set_node_attr(loc, "dZ", self.dZ)
        hfile.set_node_attr(loc, "PhiCenter", self.PhiCenter)
        hfile.set_node_attr(loc, "nRChannels", self.nRChannels)
        hfile.set_node_attr(loc, "nZChannels", self.nZChannels)
        hfile.set_node_attr(loc, "omega", self.omega)

    def dump(self, h5handle=None, h5FileName="psfChannels.h5"):
        """ Dump PSF Channel data into h5 file
              If the file name is not given as an argument, the default
              file name is instance+".h5"
      """
        close = False
        if not h5handle:
            h5handle = h5py.File(h5FileName, mode="w")
            close = True

        hf = h5handle
        hf.attrs.create("title", "PSF channel data")

        # We construct object from a yaml dict so use attrs
        memberList = [
            a
            for a in dir(self)
            if not a.startswith("__") and not callable(getattr(self, a))
        ]
        for memberName in memberList:
            member = getattr(self, memberName)
            # Print the arrays
            if memberName in self.arrayNames:
                agrp = hf.create_group(memberName)
                agrp.create_dataset("RZpts", data=member.RZpts, dtype="d")
                NR = member.nRChannels
                NZ = member.nZChannels
                agrp.attrs.create("NR", NR)
                agrp.attrs.create("NZ", NZ)
                if self.hasPsfInfo:
                    for j in range(NZ):
                        for i in range(NR):
                            psfname = "psf_" + str(i) + "_" + str(j)
                            pgrp = agrp.create_group(psfname)
                            member.psfArray[i, j].dump(h5handle=pgrp)

                # This is for the diagnostics which create the signal
                # so this is for child functions
                if hasattr(agrp, "signal"):
                    agrp.create_dataset("signal", data=member.signal,
                                        dtype="d")
                if hasattr(agrp, "field"):
                    for j in range(NZ):
                        for i in range(NR):
                            for fi in agrp.field:
                                agrp.create_dataset(
                                    "field_comp" + str(fi),
                                    data=agrp.field[fi],
                                    dtype="d",
                                )

            else:
                if member:
                    if memberName == "info":
                        for ikey in member:
                            hf.attrs.create(ikey, member[ikey])
                    else:
                        hf.attrs.create(memberName, member)

        if close:
            h5handle.close()

    def dumpH53D(self, h53dfile=None):
        """ Calculate the positions in 3D configuration
              space and then dump the data in h5 file format for easy
              visualization in a program like VISIT.
              If the file name is not given as an argument, the default
              file name is instance+"_3D.h5"
          """
        h5file = tables.open_file(h53dfile, mode="w", title="Soft X-Ray Data")
        self._dumpAttributes(h5file, h5file.root)
        for arrayName in self.arraysUsed:
            arGrp = h5file.create_group(h5file.root, arrayName,
                                        "Channel description")
            self.array[arrayName]._dumpAttributes(h5file, arGrp)
            nR = self.array[arrayName].nRChannels
            nZ = self.array[arrayName].nZChannels
            # Put into a cartesian coordinate system for plotting
            pts2D = np.zeros((nR * nZ, 2), np.float)
            pts3D = np.zeros((nR * nZ, 3), np.float)
            pointCounter = 0
            for i in range(nR):
                for j in range(nZ):
                    R = self.array[arrayName].rzpPts[0, i, j]
                    Z = self.array[arrayName].rzpPts[1, i, j]
                    p = self.array[arrayName].rzpPts[2, i, j]
                    pts3D[pointCounter, 0] = R * np.cos(p)
                    pts3D[pointCounter, 1] = R * np.sin(p)
                    pts3D[pointCounter, 2] = Z
                    pts2D[pointCounter, 0] = R * np.cos(p)
                    pts2D[pointCounter, 1] = Z
                    pointCounter = pointCounter + 1
            dataset = h5file.create_array(arGrp, "points", pts3D)
            dataset._f_setattr("vsType", "variableWithMesh")
            dataset._f_setattr("vsNumSpatialDims", 3)
            dataset = h5file.create_array(arGrp, "points_2d", pts2D)
            dataset._f_setattr("vsType", "variableWithMesh")
            dataset._f_setattr("vsNumSpatialDims", 2)
        h5file.close()


datalist = []


def returnname(name):
    if name not in datalist:
        return name
    else:
        return None


def restore(h5handle=None, h5FileName="PSFchannel.h5"):
    """
  Restore from a PSFchannels.dump() file
  """
    close = False
    if not h5handle:
        h5handle = h5py.File(h5FileName, mode="r")
        close = True
    hf = h5handle

    initDict = {}

    arrayNames = hf.attrs.get("arrayNames")
    initDict["arrayNames"] = arrayNames
    # Most of these will get put into info
    for atts in hf.keys():
        hfatts = hf.attrs.get(atts)
        if hfatts:
            initDict[atts] = hfatts

    for aname in arrayNames:
        agrp = hf.get(aname)
        ds = agrp.get("RZpts")[:, :, :]
        NR = agrp.attrs.get("NR")
        NZ = agrp.attrs.get("NZ")
        initDict[aname] = {}
        initDict[aname]["RZpts"] = ds
        initDict[aname]["NR"] = NR
        initDict[aname]["NZ"] = NZ

        while 1:
            item = agrp.visit(returnname)
            if item is None:
                break
            datalist.append(item)
            if item not in arrayNames:
                hfitem = agrp.get(item)
                if isinstance(hfitem, h5py.Dataset):
                    continue
                if "PSFs" not in initDict[aname]:
                    initDict[aname]["PSFs"] = {}
                initDict[aname]["PSFs"][item] = psf.restore(hfitem)

    newpsf = psfChannelArrays(None, restoreDict=initDict)

    if close:
        h5handle.close()
    return newpsf
