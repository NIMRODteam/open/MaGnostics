
Diagnostics based on Point Spread Functions (PSF)
=================================================

Currently, we are aware of two diagnostics based on 
Point Spread Functions (PSFs):

  * Beam Emission Spectroscopy (BES)
  
  * Electron Cyclotron Emission Imaging (ECEI)

They basic equations for calculating the signal are 
the same for both:

TODO:  Fix the equation.

.. math::    
      
    \tilde{n} = \frac{\int n(x,t) - n_o }{n_o}

The point spread function contains the information specific to a given
diagnostic. 

Because the physics of a given diagnostic is contained within the
point spread function which is provided by the diagnosticians, most of
the functionality of a given diagnostic is contained within the PSF
base class found in PSF.py.   This base class is not executable.  


Of course, what makes BES and ECEI so useful are that they have multiple
channels.  PSFchannels.py containces the PSFchannels class which derives from
the PSF class.  It handles the collection of multiple PSF locations including
multiple psf files.  

In many cases, notably co-injection, the diagnosticians will want to provide
only a single PSF file because they are all roughly the same and not worth
re-calculating for other cases.  In some cases, using an elliptoid function is
also sufficient.  Our goal here is to enable the most general cases where the
PSF functions need to be provided for each channel, but also enable workflows
where only a single PSF function is provided.

Finally, another important issue is that depending on how the simulation is set
up, there may be a requirement to shift the PSF center location for a channel,
or all channels.  We wish to enable this functionality as well.

These handle just the mechanics of dealing with the experimental data and do not
handle the evaluation of the fields from the code.  The next class then is the
"synthPSF" which is what does the *PSF-based synthetic diagnostic* and also
*synthesizes* the experimental data and code data.  

The final derived classes are in BES.py and ECEI.py.  These final classes that
customize things to their specific diagnostic.  These files are executable
because they are meant to be where the actual work is done; i.e., they are the
files that the user is meant to use.

The hierarchy then is::

      BES  ECEI
      |   \
      |    codeEval
      |
      PSFchannels
      |      \
      PSF  rzpoints

Individual documenation for each class is given below.


PSF base class
==============
