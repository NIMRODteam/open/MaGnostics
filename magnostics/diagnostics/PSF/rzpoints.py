#!/bin/env python

import os
import sys
import numpy as np

# This allows rzpointsplot.py to do imports correctly for
# reasons that are not understood
diagnostics_dir = os.path.dirname(os.path.dirname(__file__))
sys.path.append(diagnostics_dir)
magnostics_dir = os.path.dirname(diagnostics_dir)
machine_dir = os.path.join(magnostics_dir, "machine")
sys.path.append(machine_dir)

"""
Convenience class for setting a bunch of R,Z points on a
rectilinear grid.  This is the channel array used by PSFchannels
 Key output:  RZpts
"""


class diagnosticPoints:
    def __init__(self, initDict):
        """
        Create a uniform grid.

        Motivated by BES where the Ro and Zo are shot data
        so this assumes that Ro and Zo are injected into the
        initDict data that also contains the long-time scale data

        Initialization uses dictionary instead of file because
        other classes will handle file-io
        """
        for key in initDict:
            keyval = initDict[key]
            if key in ["nRChannels", "nZChannels"]:
                setattr(self, key, np.int(keyval))
            elif key in ["dR", "dZ", "Ro", "Zo", "phi"]:
                if key.endswith("_tmp"):
                    key = key.rstrip("_tmp")
                setattr(self, key, np.double(keyval))
            else:
                setattr(self, key, keyval)

        # Do some base cleanup
        if not hasattr(self, "phi"):
            self.phi = 0.0
        if not hasattr(self, "dR"):
            self.dR = 0.0
        if not hasattr(self, "dZ"):
            self.dZ = 0.0
        NR = self.nRChannels
        NZ = self.nZChannels
        self.RZpts = np.zeros((2, NR, NZ), np.double)
        rgrid = self.Ro + np.arange(0, NR) * self.dR
        zgrid = self.Zo + np.arange(0, NZ) * self.dZ
        for i in range(0, NR):
            self.RZpts[1, i, :] = zgrid
        for j in range(0, NZ):
            self.RZpts[0, :, j] = rgrid
