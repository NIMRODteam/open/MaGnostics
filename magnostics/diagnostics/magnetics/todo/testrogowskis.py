# -*- coding: utf-8 -*-
"""
Created on Sat Mar 14 22:05:16 2015

@author: hebert
"""

import rogowski
import numpy



#First, test toroidal variation of rogowski signal.  Should be constant
#n=4000
#m=21

#xyzs=numpy.zeros((m,n+1,3))

#diagnostic_list=[]

#for j in range(m):
#    phi=j*2*numpy.pi/m
#    for i in range(n+1):
#        r=0.05
#        theta=i*2*numpy.pi/n
#        xyzs[j,i,0]=((0.75+r*numpy.cos(theta))*numpy.cos(phi))
#        xyzs[j,i,1]=((0.75+r*numpy.cos(theta))*numpy.sin(phi))
#        xyzs[j,i,2]=r*numpy.sin(theta)

       
#    diagnostic_list.append(rogowski.rogowski(xyzs[j,:,0],xyzs[j,:,1],xyzs[j,:,2]))
       
#Test radial dependence
       
diagnostic_list=[]

n=10

xyzs=numpy.zeros((10,n+1,3))
for j in range(10):
    phi=numpy.pi/10
    for i in range(n+1):
        r=0.299-j*0.005
        theta=i*2*numpy.pi/n
        xyzs[j,i,0]=((0.75+r*numpy.cos(theta))*numpy.cos(phi))
        xyzs[j,i,1]=((0.75+r*numpy.cos(theta))*numpy.sin(phi))
        xyzs[j,i,2]=r*numpy.sin(theta)

       
    diagnostic_list.append(rogowski.rogowski(xyzs[j,:,0],xyzs[j,:,1],xyzs[j,:,2]))
