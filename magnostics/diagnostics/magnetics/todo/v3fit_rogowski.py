from diagnostic import diagnostic
import netCDF4 as nc
import numpy

class v3fit_rogowski(diagnostic):
    def __init__(self,ncfile):
        dataset=nc.Dataset(ncfile,'r',format='NETCDF4')
        
        #Random variables which are read in but not used
        self.xnod=dataset.variables.get("xnod")
#        self.area=dataset.variables.get("ave_n_area").getValue()[0]
        self.kp_store=dataset.variables.get("mddc_mrf_kp_store").getValue()[0]
        self.flux_factor=dataset.variables.get("mddc_desc_flux_factor").getValue()[0]
        self.n_fp=dataset.variables.get("mddc_mrf_n_field_periods").getValue()[0]
        self.stell_sym=dataset.variables.get("mddc_mrf_lstell_sym__logical__").getValue()[0]

        #Variables for recreating the magnetic response function grid
        self.rmin=dataset.variables.get("mddc_mrf_rmin").getValue()[0]
        self.rmax=dataset.variables.get("mddc_mrf_rmax").getValue()[0]
        self.zmin=dataset.variables.get("mddc_mrf_zmin").getValue()[0]
        self.zmax=dataset.variables.get("mddc_mrf_zmax").getValue()[0]
        self.ir=dataset.variables.get("mddc_mrf_ir").getValue()[0]
        self.jz=dataset.variables.get("mddc_mrf_jz").getValue()[0]
        self.kp=dataset.variables.get("mddc_mrf_kp").getValue()[0]
        
        #Magnetic responses in cylidrical R, Phi(f), and Z
        self.a_r=dataset.variables.get("mddc_mrf_a_r")
        self.a_z=dataset.variables.get("mddc_mrf_a_z")
        self.a_f=dataset.variables.get("mddc_mrf_a_f")
        #These look like they have shape (phi,z,r)        
        
        #Building the mesh
        self.delr=(self.rmax-self.rmin)/(self.ir-1)
        self.delz=(self.zmax-self.zmin)/(self.jz-1)
        self.fperiod=2*numpy.pi/self.n_fp
        self.delp=2*numpy.pi/self.kp_store
        self.rgrid=numpy.zeros(self.ir)
        self.zgrid=numpy.zeros(self.jz)
        self.rzgrid=numpy.zeros([self.ir,self.jz,2])
        self.rangerpz=numpy.zeros([self.kp_store,self.jz,self.ir,3])
        self.rangexyz=numpy.zeros([self.kp_store,self.jz,self.ir,3])
        for i in range(self.ir):
            self.rangerpz[:,:,i,0]=self.rmin+i*self.delr
        for j in range(self.jz):
            self.rangerpz[:,j,:,2]=self.zmin+j*self.delz
        for k in range(self.kp_store):
            self.rangerpz[k,:,:,1]=k*self.delp
#        for k in range(self.kp_store):
#            for j in range(self.jz):
#                for i in range(self.ir):
#                    self.rangexyz[k,j,i]=numpy.array([self.rangerpz[k,j,i,0]*numpy.cos(self.rangerpz[k,j,i,1]),self.rangerpz[k,j,i,0]*numpy.sin(self.rangerpz[k,j,i,1]),self.rangerpz[k,j,i,2]])

                    
    def diag_write(self):
        import matplotlib.pyplot as plt
        
        plt.plot(self.rangerpz)
        
    
    def PolygonArea(self,corners):
         n = len(corners) # of corners
         area = 0.0
         for i in range(n):
             j = (i + 1) % n
             area += corners[i][0] * corners[j][1]
             area -= corners[j][0] * corners[i][1]
         area = abs(area) / 2.0
         return area
    
    
    def get_signal(self,model):
        from scipy import interpolate
        sig=0
        
        self.coords=numpy.zeros([model.nimgrid.shape[0],model.nimgrid.shape[1],4,2])
        self.vol=numpy.zeros([model.nimgrid.shape[0],model.nimgrid.shape[1]])

        #Calculate volume elements for integration
        for i in range(model.nimgrid.shape[0]):
            for j in range(model.nimgrid.shape[1]):
                #Deal with special cases of being on the first or last element
                if i==0:
                    im=i
                    ip=i+1
                elif i==model.nimgrid.shape[0]-1:
                    im=i-1
                    ip=i
                else:
                    im=i-1
                    ip=i+1
                if j==0:
                    jm=-2
                    jp=j+1
                elif j==model.nimgrid.shape[1]-1:
                    jm=j-1
                    jp=1
                else:
                    jm=j-1
                    jp=j+1
                ipjp=model.nimgrid[ip,jp]
                ipjm=model.nimgrid[ip,jm]
                imjm=model.nimgrid[im,jm]
                imjp=model.nimgrid[im,jp]
                self.coords[i,j,0]=(ipjp+ipjm)/2
                self.coords[i,j,1]=(ipjm+imjm)/2
                self.coords[i,j,2]=(imjm+imjp)/2
                self.coords[i,j,3]=(imjp+ipjp)/2
                self.vol[i,j]=self.PolygonArea(self.coords[i,j])/2 #This is a kludge; shouldn't have to be like this
                 
        #Find the fractional points in V3FIT space where the NIMROD points are
        guessr=(model.nimgrid[:,:,0]-self.rmin)/self.delr
        guessz=(model.nimgrid[:,:,1]-self.zmin)/self.delz
     
        #Do the integration
        for phi in range(self.rangerpz.shape[0]):     
            #For each angle, re-interpolate the response functions for each variable
            azinterp=interpolate.RectBivariateSpline(range(self.jz),range(self.ir),self.a_z[phi])
            aphiinterp=interpolate.RectBivariateSpline(range(self.jz),range(self.ir),self.a_f[phi])
            arinterp=interpolate.RectBivariateSpline(range(self.jz),range(self.ir),self.a_r[phi])
            for i in range(model.nimgrid.shape[0]):
                for j in range(model.nimgrid.shape[1]-1):
                    az=azinterp(guessz[i,j],guessr[i,j])[0,0]
                    aphi=aphiinterp(guessz[i,j],guessr[i,j])[0,0]
                    ar=arinterp(guessz[i,j],guessr[i,j])[0,0]
                    #Get the data straight from the NIMROD representation 
                    dat=model.nimrod_fourier("J",i,j,-phi*self.delp)
#                    sig+=self.vol[i,j]
                    #Do the dot product and the integration
                    sig+=(ar*dat[0]+az*dat[1]-aphi*dat[2])*self.vol[i,j]*self.delp*model.nimgrid[i,j,0]


        return sig
                    
                    
                    