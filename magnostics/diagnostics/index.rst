
Diagnostic development
~~~~~~~~~~~~~~~~~~~~~~~~


There are two generic steps of developing a synthetic diagnostic::

      1. Specify geometry
      2. Evaluate function based on code data using geometry

Specifying the geometry can be surprisingly tedious and annoying.  It is useful
to develop this step independently before worrying about evaluating your code
data.  Interactivity, and visualizing the geometry should be done before
worrying about the details of the diagnostic.


rzpoints.py
===========

The basic class for specifying geometry is the RZpoints in rzpoints.py.

.. module:: rzpoints

.. autofunction::  __init__

