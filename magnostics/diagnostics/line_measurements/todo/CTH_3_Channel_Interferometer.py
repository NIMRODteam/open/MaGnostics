# -*- coding: utf-8 -*-
"""
Created on Wed Aug 20 13:08:55 2014

@author: hebert

Function to add CTH 3 channel inteferometer to a diagnostic list for use with MISD.
If passed a diagnostic list, will add to the end of it.  If passed a variable list, will add "nd" as a variable unless it is already in the list.
"""

#Define CTH 3 channel inteferometer for use with MISD

def add(diagnostic_list=None,variable_list=None):

    import line_integrated_density
    import numpy
    
    if variable_list:
        if variable_list.count("nd")==0:
            variable_list.append("nd")
    else:
        variable_list=["nd"]
    
    phi=108*numpy.pi/180
    npoints=100
    for zpos in [0.107,0,-0.106]:
        r=numpy.sqrt(0.3**2-zpos**2)
        x=(r+0.75)*numpy.cos(phi)
        y=(r+0.75)*numpy.sin(phi)
        xnode=numpy.zeros(npoints)
        ynode=numpy.zeros(npoints)
        znode=numpy.zeros(npoints)
        for i in range(xnode.shape[0]):
            xnode[i]=x-2*r*numpy.cos(phi)*i/(xnode.shape[0]*1.0)
            ynode[i]=y-2*r*numpy.sin(phi)*i/(ynode.shape[0]*1.0)
            znode[i]=zpos
        if diagnostic_list:
            diagnostic_list.append(line_integrated_density.single_pass(xnode,ynode,znode))
        else:
            diagnostic_list=[line_integrated_density.single_pass(xnode,ynode,znode)]

    return diagnostic_list,variable_list