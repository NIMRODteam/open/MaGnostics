# -*- coding: utf-8 -*-

import numpy

class single_pass():
    def __init__(self,xnodes,ynodes,znodes):
        if xnodes.shape[0]!=ynodes.shape[0]:
            raise("xnodes and ynodes have incompatable shapes; all inputs must have same length")
        elif xnodes.shape[0]!=znodes.shape[0]:
            raise("xnodes and znodes have incompatable shapes; all inputs must have same length")
        elif ynodes.shape[0]!=znodes.shape[0]:
            raise("ynodes and znodes have incompatable shapes; all inputs must have same length")
        else:
            self.xnodes=xnodes
            self.ynodes=ynodes
            self.znodes=znodes
            self.signal=None
    
    def get_signal(self,model):
        import numpy
        import matplotlib.pyplot as plt
        import heapq
        self.dat=numpy.zeros([self.xnodes.shape[0]-1,2])
        sig=0
        for i in range(1,self.xnodes.shape[0]):
            delta=numpy.sqrt((self.xnodes[i]-self.xnodes[i-1])**2+(self.ynodes[i]-self.ynodes[i-1])**2+(self.znodes[i]-self.znodes[i])**2)
            sig=model.get_data("nd",self.xnodes[i],self.ynodes[i],self.znodes[i])*delta
            self.dat[i-1,0]=self.xnodes[i]
            self.dat[i-1,1]=model.get_data("nd",self.xnodes[i],self.ynodes[i],self.znodes[i])*delta
#        try:
#            self.idx.__class__
#        except:
#            self.idx=numpy.where(self.dat[:,1]-numpy.min(heapq.nlargest(40,self.dat[:,1]))>0)[0]
#        for i in self.idx:
#            self.dat[i,0]=numpy.sqrt((self.xnodes[i])**2+(self.ynodes[i])**2+(self.znodes[i])**2)
#        self.signal=numpy.sum(self.dat[self.idx,1])*0.6
        self.signal=numpy.sum(self.dat)/numpy.linalg.norm(numpy.array([self.xnodes[0]-self.xnodes[-1],self.ynodes[0]-self.ynodes[-1],self.znodes[0]-self.znodes[-1]]))
#        plt.plot(self.dat[:,0],numpy.log10(self.dat[:,1]))
#        plt.show()
        return self.signal
            
        