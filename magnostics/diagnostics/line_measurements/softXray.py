#!/bin/env python

import os
import numpy
from configobj import ConfigObj
import tables
from matplotlib import pylab

# Import simple wall to find intersects
from simpleWall import *

class singleChannel:
    def __init__(self,i,Rc,Zc,Pc,Ac):
        self.number=i+1                 # Start with 1 as the counter
        self.R=Rc
        self.Z=Zc
        self.Phi=Pc
        self.Angle=Ac*2.*numpy.pi/360.  # Convert to radians

    def makeLines(self,nPoints,Rmn,Zmn,Rmx,Zmx):
          """ Define a set of points that cuts across domain starting with the channel 
              location to other side of box defined as passed in.
              I wish I done a different algorithm based on intersection
              of line segments.  Refactor later
          """
          # Figure out if it is in upper left or lower right 
          bottom=False; top=False; left=False; right=False
          if self.R < Rmn: bottom=True
          if self.Z < Zmn: left=True
          if self.R > Rmx: top=True
          if self.Z > Zmx: right=True

          # Define parameters of line: Z=m R + b
          if numpy.abs(self.Angle - numpy.pi/2.) > numpy.float(1.e-8):
              m=numpy.tan(self.Angle)
              b=self.Z - m* self.R
          else:
              m=-9999999999.
              b=-9999999999.

          Zntmn=m*Rmn + b
          Zntmx=m*Rmx + b
          Rntmn=(Zmn - b)/m
          Rntmx=(Zmx - b)/m

          RZbox=numpy.zeros((2,2),numpy.float)
          i=0
          # Make it
          if Zntmn > Zmn and Zntmn< Zmx:
              RZbox[0,i]=Rmn; RZbox[1,i]=Zntmn
              if bottom: RZbox[0,i]=self.R; RZbox[1,i]=self.Z
              i=i+1
          if Zntmx > Zmn and Zntmx < Zmx:
              RZbox[0,i]=Rmx; RZbox[1,i]=Zntmx
              if top: RZbox[0,i]=self.R; RZbox[1,i]=self.Z
              i=i+1
          if Rntmn > Rmn and Rntmn< Rmx:
              RZbox[1,i]=Zmn; RZbox[0,i]=Rntmn
              if left: RZbox[0,i]=self.R; RZbox[1,i]=self.Z
              i=i+1
          if Rntmx > Rmn and Rntmx < Rmx:
              RZbox[1,i]=Zmx; RZbox[0,i]=Rntmx
              if right: RZbox[0,i]=self.R; RZbox[1,i]=self.Z
              i=i+1
          self.RZbox=RZbox

          # Now define a set of li
          self.RZpts=numpy.zeros((2,nPoints),numpy.float)
          dR=RZbox[0,1]-RZbox[0,0]; dZ=RZbox[1,1]-RZbox[1,0]
          for i in range(nPoints):
            di=numpy.float(i)/numpy.float(nPoints-1)
            self.RZpts[0,i]=RZbox[0,0]+di*dR
            self.RZpts[1,i]=RZbox[1,0]+di*dZ
               
class softXray:
    def __init__(self,configFile):

        # Get the data which is in a configObj format
        if os.path.exists(configFile):
           swConfig = ConfigObj(configFile)
        else:
           print "Error: "+configFile+ "not found."
           return 0
        self.channelLinesCalled=False
        # There is only one key
        self.configFile=configFile
        self.swInstance=swConfig.keys()[0]
        self.machine=swConfig[self.swInstance]["machine"]
        self.nChannels=numpy.int(swConfig[self.swInstance]["nChannels"])
        self.nChannelsUse=self.nChannels
        nc=self.nChannels

        RsList=swConfig[self.swInstance]["Rsxr"]
        ZsList=swConfig[self.swInstance]["Zsxr"]
        PsList=swConfig[self.swInstance]["PhiSxr"]
        AsList=swConfig[self.swInstance]["AngSxr"]

        ## Unfortunately configObj lists don't come in with members as
        # numbers so we have to convert each member.  Save original data
        self.Rchannel = numpy.zeros( nc, numpy.float)
        self.Zchannel = numpy.zeros( nc, numpy.float)
        self.AngleChannel = numpy.zeros( nc, numpy.float)
        self.PhiChannel   = numpy.zeros( nc, numpy.float)
        self.channel=[]
        for i in range(nc):
          Rc=numpy.float(RsList[i])
          Zc=numpy.float(ZsList[i])
          Pc=numpy.float(PsList[i])
          Ac=numpy.float(AsList[i])
          self.Rchannel[i]=Rc;      self.Zchannel[i]=Zc
          self.AngleChannel[i]=Ac;  self.PhiChannel[i]=Pc 
          self.channel.append(singleChannel(i,Rc,Zc,Pc,Ac))

    def _strChName(self,iCh):
       """ Given a channel number return a string that makes the h5ls look nice
       """
       if iCh < 10: 
         ChName="00"+str(iCh)
       elif iCh<100: 
         ChName="0"+str(iCh)
       else: 
         ChName=str(iCh)
       return ChName

    def printStdout(self):
          """ Print the data to standard output."""
          print "nWall ", self.nChannelsUse
          print "Ch. Name    R     Z      Angle         Phi"
          for i in range(self.nChannelsUse):
            ch=self.channel[i]
            chNm="Channel_"+self._strChName(ch.number)
            print chNm, ch.R, ch.Z, ch.Angle, ch.Phi

    def channelFilter(self,iChDel):
          """ Delete a channel from the original list
          """
          for i in range(self.nChannelsUse):
              if self.channel[i].number==iChDel:
                  self.channel.remove(self.channel[i])
                  self.nChannelsUse=self.nChannelsUse-1
                  return
          return

    def makeChannelLines(self,nPoints,wallConfigFile):
          """ Create a set of points that can be used for
             plotting the location of the soft Xray chords
             It takes the box file from a simple wall config file
          """
          self.channelLinesCalled=True
          wall=simpleWall(wallConfigFile)
          R1=wall.Rwall.min(); R2=wall.Rwall.max(); Z1=wall.Zwall.min(); Z2=wall.Zwall.max()
          for i in range(self.nChannelsUse):
            self.channel[i].makeLines(nPoints,R1,Z1,R2,Z2)


    def plot2d(self,wallConfigFile=None):
          """ Plot just the R,Z locations and ignore phi
          """ 
          if wallConfigFile:
             wall=simpleWall(wallConfigFile)
             self.makeChannelLines(5,wallConfigFile)
          else:
            if not self.channelLinesCalled:
              print "Cannot plot points without makeChannelLines called"
              return

          pylab.axis('equal')
          pylab.xlabel('R (m)')
          pylab.ylabel('Z (m)')
          for i in range(self.nChannelsUse):
            pylab.plot(self.channel[i].RZpts[0,:],self.channel[i].RZpts[1,:])
          xrayTitle="Soft X-Ray choords from "+self.swInstance
          pylab.title(xrayTitle)
          if wallConfigFile:
             pylab.plot(wall.Rwall,wall.Zwall, linestyle="-", lw=3., color="black")
          pylab.show()

    def _dumpAttributes(self,hfile,loc):
          """ Write the attributes to an h5file """
          hfile.setNodeAttr(loc,"instance", self.swInstance)
          hfile.setNodeAttr(loc,"nChannels", self.nChannels)
          hfile.setNodeAttr(loc,"nChannelsUse", self.nChannelsUse)
          hfile.setNodeAttr(loc,"configFile", self.configFile)
 
    def dumpData(self,h52dfile=None,wallConfigFile=None):
          """ Dump soft Xray data into h5 file 
              If the file name is not given as an argument, the default
              file name is swInstance+".h5"
          """
          if wallConfigFile:
             wall=simpleWall(wallConfigFile)
             self.makeChannelLines(5,wallConfigFile)

          if not h52dfile:
                h52dfile=self.swInstance+".h5"
          h5file=tables.openFile(h52dfile, mode="w", title="Soft X-Ray Data")
          self._dumpAttributes(h5file,h5file.root)
          for i in range(self.nChannelsUse):
            ch=self.channel[i]
            chNm="Channel_"+self._strChName(ch.number)
            chGrp = h5file.createGroup(h5file.root, chNm, 'Channel description')
            h5file.createArray(chGrp,'R', ch.R)
            h5file.createArray(chGrp,'Z', ch.Z)
            h5file.createArray(chGrp,'Phi', ch.Phi)
            h5file.createArray(chGrp,'Angle', ch.Angle)
            if self.channelLinesCalled:
                h5file.createArray(chGrp,'Rpts',ch.RZpts[0,:])
                h5file.createArray(chGrp,'Zpts',ch.RZpts[1,:])
          h5file.close()


    def dumpH53D(self,h53dfile=None,wallConfigFile=None,nPoints=None):
          """ Calculate the positions in of a wall in 3D configuration
              space and then dump the data in h5 file format for easy 
              visualization in a program like VISIT. 
              The number of points to use in a slice is given as an argument:
              npoints.  
              If the file name is not given as an argument, the default
              file name is swInstance+"_3D.h5"
          """
          if wallConfigFile:
             wall=simpleWall(wallConfigFile)
             if not nPoints: nPoints=5
             self.makeChannelLines(nPoints,wallConfigFile)
          else:
            if not self.channelLinesCalled:
              print "Cannot dump 3D points without makeChannelLines called"

          if not h53dfile:
                h53dfile=self.swInstance+"_3D.h5"
          h5file=tables.openFile(h53dfile, mode="w", title="Soft X-Ray Data")
          self._dumpAttributes(h5file,h5file.root)
          pts3D = pylab.zeros( (nPoints,3), numpy.float)
          connectivity = numpy.zeros( (nPoints-1,2), dtype=numpy.int32)
          for j in range(nPoints):
            if j != nPoints-1:
              connectivity[j,0]=j; connectivity[j,1]=j+1;
          h5file.createArray(h5file.root,'connectivity', connectivity)
          for i in range(self.nChannelsUse):
            ch=self.channel[i]
            chNm="Channel_"+self._strChName(ch.number)
            chGrp = h5file.createGroup(h5file.root, chNm, 'Channel description')
            # Put into a cartesian coordinate system for plotting
            phi=ch.Phi
            pts3D[:,0]=ch.RZpts[0,:]*numpy.cos(phi)
            pts3D[:,1]=ch.RZpts[0,:]*numpy.sin(phi)
            pts3D[:,2]=ch.RZpts[1,:]
            h5file.createArray(chGrp,'points', pts3D)
            h5file.setNodeAttr(chGrp,"vsType",   "mesh")
            h5file.setNodeAttr(chGrp,"vsKind",   "unstructured")
            h5file.setNodeAttr(chGrp,"vsPoints", "points")
            h5file.setNodeAttr(chGrp,"vsLines",  "/connectivity")
            h5file.setNodeAttr(chGrp,"vsMD", "allChannels")
          h5file.close()

def main():
    parser = optparse.OptionParser(usage="%prog [options] configFile")
    parser.add_option('-i', '--input', dest='input',
                      help='Name of soft XRay configuration file if not specified as argument.',
                      default='')
    parser.add_option('-p', '--print', dest='doPrint',
                      help='Print all possible fields to standard out along with description',
                      action='store_true')
    parser.add_option('-l', '--plot', dest='doPlot',
                      help='Make 2D plot of the simple wall', action='store_true')
    parser.add_option('-a', '--all', dest='doAll', action='store_true',
                      help='Write out all hdf5 output files (using default names if not specified by other options).',
                      default='')
    parser.add_option('--h52D', dest='h52Dname', default='',
                      help='Name of 2D output file (.h5 format).')
    parser.add_option('--h53D', dest='h53Dname', default='',
                      help='Name of HDF5 3D file to write data')
    parser.add_option('-n','--npoints', dest='npoints', default='100',
                      help='Number of points to use in writing HDF5 3D file')
    parser.add_option('-w','--wallfile', dest='wallFile', default='',
                      help='Name of wall file to determine the chords')

    options, args = parser.parse_args()

    # Too many arguments
    if len(args) > 1:
      parser.print_usage()
      return
    elif len(args) == 1:
      configFile=args[0]
    # No arguments
    else:
      if options.input == '':
        print "Must specify an configuration file"
        return
      else:
        configFile=options.input

    sxr=softXray(configFile)

    # If output not written, then just write to stdout:
    if options.doPrint:
      sxr.printStdout()

    printH52D=False; printH53D=False
    if options.doAll: 
      printH52D=True; printH53D=True

    nPointsIn=numpy.int(options.npoints)

    if options.h52Dname: 
        sxr.dumpData(h52dfile=h52Dname)
    elif printH52D:
        sxr.dumpData()
    if options.h53Dname: 
        if options.wallFile == '': 
           print " Cannot dump 3D data without wall file specified (-w) "
           return
        sxr.dumpH53D(h53dfile=options.h53Dname,nPoints=nPointsIn,wallConfigFile=options.wallFile)
    elif printH53D:
        if options.wallFile == '': 
           print " Cannot dump 3D data without wall file specified (-w) "
           return
        sxr.dumpH53D(nPoints=nPointsIn,wallConfigFile=options.wallFile)

    if options.doPlot: 
        if options.wallFile == '': 
           print " Cannot plot without wall file specified (-w) "
           return
        sxr.plot2d(wallConfigFile=options.wallFile)

#        for i in range(61,65):
#            print "deleting channel ", i+1
#            sxr.channelFilter(i)
      

if __name__ == "__main__":
        main()
