# -*- coding: utf-8 -*-
"""
Created on Thu Sep  4 13:37:34 2014

@author: hebert
"""

class point_pressure():
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        
    def get_signal(self,model):
        import numpy
        f=open("/Users/hebert/Desktop/p50000","w")
        if isinstance(self.x,(list,numpy.ndarray)):
            dat=numpy.zeros(self.x.shape[0])
            f.write("{")
            for i in range(self.x.shape[0]):
                dat[i]=model.get_data("p",self.x[i],self.y[i],self.z[i])
                f.write("{"+str(self.x[i])+","+str(self.y[i])+","+str(self.z[i])+","+str(dat[i])+"}")
                if i!= self.x.shape[0]-1:
                    f.write(",")
                
            f.write("}")
           
        elif isinstance(self.x,(float,int)):
            print self.x, self.y, self.z, model.get_data("p",self.x,self.y,self.z)[0]
            
        f.close()

class point_B():
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
        
    def get_signal(self,model):
        import numpy
#        f=open("/Users/hebert/Desktop/b50000","w")
        if isinstance(self.x,(list,numpy.ndarray)):
            dat=numpy.zeros(self.x.shape[0])
#            f.write("{")
            for i in range(self.x.shape[0]):
                dat[i]=numpy.linalg.norm(model.get_data("B",self.x[i],self.y[i],self.z[i]))
#                f.write("{"+str(self.x[i])+","+str(self.y[i])+","+str(self.z[i])+","+str(dat[i])+"}")
#                if i!= self.x.shape[0]-1:
#                    f.write(",")
                
#            f.write("}")
           
        elif isinstance(self.x,(float,int)):
#            f.write("{")
#            f.write(str(model.get_data("B",self.x,self.y,self.z)[0]))
            return model.get_data("B",self.x,self.y,self.z)
#            f.write("}")

#        f.close()