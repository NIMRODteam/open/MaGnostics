from diagnostic import diagnostic
import csv
import numpy

class D3D_THOM(diagnostic):
    """DIII-D Thomson scattering diagnostic.  To initialize:
    
    D3D_THOM(infile)
    
    infile  :  Experimental csv file containing evaluation points
    
    
    """
    def __init__(self,infile,varname):
        """Parameters:
        
        infile  :  Experimental csv file containing evaluation points, assumed to be in column 4(R) and 5(Z)
        """
        #Initialize columns from CSV file
        self.psi=numpy.zeros([])
        self.t=numpy.zeros([])
        self.terr=numpy.zeros([])
        self.r=numpy.zeros([])
        self.z=numpy.zeros([])
        self.varname=varname
        rownum=0
        with open(infile,'r') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                if rownum==2:
                    self.psi=numpy.array(float(row[0]))
                    self.t=numpy.array(float(row[1]))
                    self.terr=numpy.array(float(row[2]))
                    self.r=numpy.array(float(row[3]))
                    self.z=numpy.array(float(row[4]))
                if rownum>2:
                    self.psi=numpy.append(self.psi,float(row[0]))
                    self.t=numpy.append(self.t,float(row[1]))
                    self.terr=numpy.append(self.terr,float(row[2]))
                    self.r=numpy.append(self.r,float(row[3]))
                    self.z=numpy.append(self.z,float(row[4]))
                rownum+=1
        #Convert temperatures keV->eV
        if (varname is "te" or varname is "ti"):
          self.t=self.t*1000
          self.terr=self.terr*1000
        if (varname is "nd"):
          self.t=self.t*1e20
          self.terr=self.terr*1e20
    
    def get_signal(self,model):
        self.dat=numpy.zeros(len(self.r))
        for i in range(len(self.r)):
            self.dat[i]=model.get_data(self.varname,0,self.r[i],self.z[i])
            
        return self.dat
