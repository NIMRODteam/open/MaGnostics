#!/usr/bin/env python
import numpy as np
import os
import yaml

"""
Example class methods that should be supported by any code field evaluation
This is derived from `nimrod_eval.py`.

The two most basic methods are:

 `def __init__(self, dumpfile, *args, **kwargs)`

    Constructor that requires a dumpfile and then additional arguments that
    control the evaluation.

  def eval_field(self, field, rzp, dmode=0, eq=2):

      The following features must be implemented::

          o The quantity is specified by field (n, T, b, j, ...)
          o rzp must be multidimensional and support for point, 1D grids,
             2D grids, and 3D grids must be implemented.  Private methods
             are used for these different dimensions.  The point in supporting
             these different methods is that the solution of the prior point
             serves as a good initial guess for the previous point and this
             greatly improves performance.
             The nomenclature is:
                    (3)                : single point
                    (3, nx1)           : line
                    (3, nx1, nx2)      : plane
                    (3, nx1, nx2, nx3) : cube
          o rzp must support either cartesian or cylindrical coordinate systems
          o dmode is a parameter on whether to return the first or second
            derivatives as well
                      0 : field only
                      1 : field + first derivatives
                      2 : field + first derivatives + second derivatives
          o eq controls whether to add the equilibrium or not  (common
             in MHD codes)
                   3 : equilibrium + n0 perturbation
                   2 : equilibrium + perturbation
                   1 : equilibrium
                   0 : perturbation
        :return: array of field values (nqty, rzp.shape)
"""


class EvalCode:
    """
    Read a dump file and evaluate fields at RZPhi locations
    """

    def __init__(self, dumpfile, fieldlist="bvnpf", coord="rzp"):
        """
        Initialize datastructure for code evales
        """

        # Handle dump file initialization.  For the exemplar, there is a yaml
        # file in develop that we use:
        if not os.path.exists(dumpfile):
            raise Exception("Dump file does not exist: " + dumpfile)

        self.dumpfile = dumpfile
        # Here evalobj is a member.
        # With ctypes, it's generally a global (equiv. to import)
        with open(dumpfile) as filestr:
            self.evalobj = yaml.load(filestr, Loader=yaml.Loader)

        # intialize stored guess (array of size 2)
        self.xyguess = 0.0

        # set the coordinate system
        if coord == "rzp":
            self.__transform = False
        elif coord == "xyz":
            self.__transform = True
        else:
            raise Exception("Unsupported coordinate system.")
        self.fieldToYaml = self.__getFieldToYaml__()

    def __getFieldToYaml__(self):
        """
        We have descriptive names in the yaml file, but we use the
        single-character interface similar to NIMROD.  This creates the
        mapping.
        """
        fieldToYaml = {}
        fieldToYaml["n"] = "density"
        fieldToYaml["b"] = "magnetic_field"
        fieldToYaml["t"] = "temperature"
        fieldToYaml["v"] = "velocity"
        fieldToYaml["p"] = "pressure"
        return fieldToYaml

    def __rzp_to_xyz(self, rzp_coord):
        """
        transforming rzp coordinate system to xyz coordinate system
        :param rzp_coord: coordinates of grid points in (R, Z, Phi)
        :return: coordinates of grid points in (X, Y, Z)
        """

        # create xyz array with the same dimension as rzp
        xyz_coord = np.zeros(np.shape(rzp_coord))

        # transform the coordinate system
        xyz_coord[0] = rzp_coord[0] * np.cos(rzp_coord[2])
        xyz_coord[1] = rzp_coord[0] * np.sin(rzp_coord[2])
        xyz_coord[2] = rzp_coord[1]

        return xyz_coord

    def __xyz_to_rzp(self, xyz_coord):
        """
        transforming xyz coordinate system to rzp coordinate system
        :param xyz_coord: coordinates of grid points in (X, Y, Z)
        :return: coordinates of grid points in (R, Z, Phi)
        """

        # create rzp array with the same dimension as xyz
        rzp_coord = np.zeros(np.shape(xyz_coord))

        # transform the coordinate system
        rzp_coord[0] = np.sqrt(xyz_coord[0] ** 2 + xyz_coord[1] ** 2)
        rzp_coord[1] = xyz_coord[2]
        rzp_coord[2] = np.arctan2(xyz_coord[1], xyz_coord[0])

        return rzp_coord

    def __eval_field_on_point(self, xyguess, field, rzp,
                              fsize, nqty, dmode, eq):
        """
        evaluate the specified field at a single point given by rzp
        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of the point in R-Z-Phi coordinate
                    (array with shape(3))
        :return: value of the field at point rzp
                 (nan if point is out of bound)

        For this routine, the yaml load just gives a single value.
        See `data/test/dump_exemplar.yml`
        """
        fieldName = self.fieldToYaml[field]
        res = self.evalobj[fieldName]
        return res

    def __eval_field_on_line(self, field, rzp, fsize, nqty, dmode, eq, nr):
        """
        evaluate the specified field along a line given by rzp
        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of all the points on line in R-Z-Phi coordinate
                    (array with shape(3, nr))
        :return: value of the field along the line defined by rzp
                 (nan if point is out of bound)
        """

        # initialize the result with zeros
        res = np.zeros([fsize, nr])

        # call the eval_field at each point
        for index in range(nr):
            res[:, index] = self.__eval_field_on_point(
                self.xyguess, field, rzp[:, index], fsize, nqty, dmode, eq
            )

        return res

    def __eval_field_on_2d_grid(self, field, rzp, fsize, nqty,
                                dmode, eq, nr, nz):
        """
        evaluate the specified field on a 2d grid given by rzp
        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of all the points on grid in R-Z-Phi coordinate
                    (array with shape(3, nr, nz))
        :return: value of the field on the 2d grid defined by rzp
                 (nan if point is out of bound)
        """

        # initialize the result with zeros
        res = np.zeros([fsize, nr, nz])

        # call each eval_field_on_line for each line
        for index in range(nz):
            res[:, :, index] = self.__eval_field_on_line(
                field, rzp[:, :, index], fsize, nqty, dmode, eq, nr
            )

        return res

    def __eval_field_on_3d_grid(self, field, rzp, fsize, nqty,
                                dmode, eq, nr, nz, np):
        """
        evaluate the specified field on a 3d grid given by rzp
        :param field: desired field given by a single character, e.g. n b v j
        :param rzp: coordinates of all the points on grid in R-Z-Phi coordinate
                    (array with shape(3, nr, nz, np))
        :return: value of the field on the 3d grid defined by rzp
                 (nan if point is out of bound)
        """

        # initialize the result with zeros
        res = np.zeros([fsize, nr, nz, np])

        # call each eval_field_on_2d_grid for each plane
        for index in range(np):
            res[:, :, :, index] = self.__eval_field_on_2d_grid(
                field, rzp[:, :, :, index], fsize, nqty, dmode, eq, nr, nz
            )

        return res

    def eval_field(self, field, rzp, dmode=0, eq=2):
        """
        main function to evaluate a field at rzp point(s)
        :param field: the specified field
        :param rzp: coordinate of the points in following format:
                    (3)                : single point
                    (3, nx1)           : line
                    (3, nx1, nx2)      : plane
                    (3, nx1, nx2, nx3) : cube
        :param dmode: order of derivatives required
                      0 : field only
                      1 : field + first derivatives
                      2 : field + first derivatives + second derivatives
        :param eq: integer value determining equilibrium and perturbation part
                   3 : equilibrium + n0 perturbation
                   2 : equilibrium + perturbation
                   1 : equilibrium
                   0 : perturbation
        :return: array of field values (nqty, rzp.shape)
        """
        # set number of components for each field
        nqty = 1
        if field in ["b", "v", "j", "f", "e"]:
            nqty = 3

        # set number of components for each field considering the
        # derivatives and the order itself
        if dmode == 0:
            fsize = nqty
            dmode = 0
        elif dmode == 1:
            fsize = nqty * 4
            dmode = 1
        elif dmode == 2:
            fsize = nqty * 10
            dmode = 2
        else:
            raise Exception("incorrect order of derivative.")

        # set the grid size
        if rzp.ndim > 1:
            nr = rzp.shape[1]
        if rzp.ndim > 2:
            nz = rzp.shape[2]
        if rzp.ndim > 3:
            np = rzp.shape[3]

        # transform the grid if needed
        if self.__transform:
            rzp = self.__xyz_to_rzp(rzp)

        # call eval_* functions according to dimension
        dimension = rzp.ndim - 1
        if dimension == 0:
            fval = self.__eval_field_on_point(
                self.xyguess, field, rzp, fsize, nqty, dmode, eq
            )
        elif dimension == 1:
            fval = self.__eval_field_on_line(field, rzp, fsize, nqty, dmode,
                                             eq, nr)
        elif dimension == 2:
            fval = self.__eval_field_on_2d_grid(
                field, rzp, fsize, nqty, dmode, eq, nr, nz
            )
        else:
            fval = self.__eval_field_on_3d_grid(
                field, rzp, fsize, nqty, dmode, eq, nr, nz, np
            )

        return fval


def main():
    """
    This is an example of usage
    """
    coord = "rzp"
    dumpfile = "../data/test/dump_exemplar.yml"
    codeeval = EvalCode(dumpfile, fieldlist="nvbp", coord=coord)

    nr = 300
    nz = 200
    rcenter = 1.7
    zcenter = 0.0
    rlen = 1.6
    zlen = 3.0
    rg = rcenter + rlen * (np.linspace(0, 1, nr) - 0.5)
    zg = zcenter + zlen * (np.linspace(0, 1, nz) - 0.5)
    grid = np.meshgrid(rg, zg, indexing="ij")

    # Convert to complete np array rather than list of arrays
    rzp = np.zeros((3, nr, nz), dtype="double")
    rzp[0, :, :] = grid[0]
    rzp[1, :, :] = grid[1]
    rzp[2, :, :] = 0.0

    field = codeeval.eval_field("n", rzp, dmode=0, eq=0)
    print(field)

    return


if __name__ == "__main__":
    main()
