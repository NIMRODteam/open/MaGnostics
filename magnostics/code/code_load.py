"""
The implementation goal is that the python script for evaluating fields at a
point are handled strictly by the codes themselves.  This rules out having a
base class define the interface, but rather we use documentation to define the
interface.  See _`code_example.py` for the basic interface.

With the code evaluation python module file handled in a separate repository,
here we handle loading the code gracefully, and populating the name space so
that a uniform interface is presented.

For example, a NIMROD user would do something like:
    import eval_nimrod
    myEval = eval_nimrod.nimrodEval(codeDumpFile, "n")
    fieldAtPt = myEval.eval_field(field, rzp)

In magnostics, we do this:
    import code_load
    myEval = code_load.codeEval("nimrod",codeDumpFile,"n")
    fieldAtPt = myEval.eval_field(field, rzp)
"""


def codeEval(codeName, dumpFileName, *args, **kwargs):
    """
    Factory function for returning the class corresponding to codeName

    codeName and dumpFileName are required, but each code can specify how to
    control the evaluation independenty so just the *args, **kwargs
    mechanism is used.  For example, NIMROD uses the "fieldlist" string
    to specify which fields to be included in the evaluation.
    """
    if codeName == "NIMROD":
        try:
            import eval_nimrod

            return eval_nimrod.EvalNimrod(dumpFileName, kwargs)
        except:
            raise Exception("Error in importing eval_nimrod for NIMROD.")
    elif codeName == "exemplar":
        import code_exemplar as codex

        return codex.EvalCode(dumpFileName, kwargs)
    else:
        raise Exception("Unsupported code.")
