#!/bin/env python

import os
import sys

# This is to handle python3 changes to how import paths work
# This is to allow calling this file directly, otherwise I would
# use the import .PSF syntax
file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)
diagnostics_dir = os.path.dirname(file_dir)
sys.path.append(diagnostics_dir)

import glob
import collections
import numpy as np
import re
import yaml

class BES(PSFchannels.PSFchannels):
    def calcSignal(self, codeDumpFile):
        # TODO:  make code agnostic
        codeEval = evalField.nimrodEval(codeDumpFile, "n")

        # TODO -- fold in multiprocessing
        for aname in self.arrayNames:
            array = getattr(self, aname)
            NR = array.NR
            NZ = array.NZ
            phi = array.phi
            array.signal = np.zeros([NR, NZ])
            array.field = np.empty((NR, NZ), dtype=object)
            # Add mp stuff
            for j in range(NZ):
                for i in range(NR):
                    psf = array.psfArray[i, j]
                    density = self.get_density(psf.grid, phi, codeEval)
                    signal[i, j] = psf.integrate(density * psf.values)
                    array.signal[i, j] = signal[i, j] / psf.integrate(psf.values)
                    # Cache the values that we evaluated via field
                    array.field[i, j] = [density]

    def get_density(self, rzGrid, phi, codeEval):
        """
        Get the density at each rzgrid location
        """
        rr, zz = rzGrid
        nrg, ngz = rr.shape()
        values = np.zeros([nrg, ngz])
        for iz in range(ngz):
            for ir in range(ngr):
                density[ir, iz] = codeEval((rr[ir, iz], zz[ir, iz], phi))
        return density


def main():
    parser = optparse.OptionParser(usage="%prog [options] geomFile codeDumpFile")
    parser.add_option(
        "-s", "--shotFile", dest="shotFile", default=None, help="File with shot data"
    ),
    parser.add_option(
        "-p", "--psfFile", dest="psfFile", default=None, help="File with psf data"
    ),
    parser.add_option(
        "-d", "--psfDir", dest="psfDir", default=None, help="File with psf data"
    ),
    parser.add_option(
        "-D",
        "--dump",
        dest="dump",
        action="store_true",
        help="Dump the data instead of plotting",
    ),
    parser.add_option(
        "-r",
        "--restore",
        dest="restorefile",
        default=None,
        help="Restore from a given file",
    ),
    parser.add_option(
        "-l",
        "--num-threads",
        dest="nThreads",
        default=1,
        help="Number of threads for parallel computation",
        type=int,
    )
    options, args = parser.parse_args()

    if not options.restorefile:
        if len(args) > 2 or len(args) < 2:
            parser.print_usage()
            return
        else:
            geomFile = args[0]
            codeDumpFile = args[1]
            if not os.path.exists(geomFile):
                print("Error: file does not exist, ", geomFile)
                return
            if not os.path.exists(codeDumpFile):
                print("Error: file does not exist, ", geomFile)
                return

    if options.shotFile:
        if not os.path.exists(options.shotFile):
            print("Error: Shot file does not exist, ", options.shotFile)
            return

    if options.psfFile:
        if not os.path.exists(options.psfFile):
            print("Error: PSF file does not exist, ", options.psfFile)
            return

    if options.psfDir:
        if not os.path.exists(options.psfDir):
            print("Error: PSF directory does not exist, ", options.psfDir)
            return

    if options.restorefile:
        if not os.path.exists(options.restorefile):
            print("File does not exist: ", options.restorefile)
            return
        myBES = PSFchannels.restore(restorefile=options.restorefile)

    else:

        myBES = BES(geomFile)

        if options.shotFile or options.psfFile or options.psfDir:
            BES.configShot(
                options.shotFile, psfFile=options.psfFile, psfDir=options.psfDir
            )

    myBES = BES.calcSignal(codeDumpFile)

    myBES.dump()

    return


if __name__ == "__main__":
    main()
