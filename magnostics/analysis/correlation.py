#!/usr/bin/env python
"""
Computes cross-correlation and cross-power between two signals computed using the 
BES-diff.py script. Reference BES sygnals are defined by the reference1 and reference2 variables.
This script reads a collection of dsym*.vsh5 that are generated using the BES-diff.py script. It 
generates a set of corresponding csyn*.vsh5 files.
"""
import numpy as np
import tables
import glob
import re
import matplotlib.pyplot as plt
#
# Cross-correlation channels that are used for the analysis 
reference1 = [5, 4]
reference2 = [5, 5]
#
FILTERS = tables.Filters(complib='zlib', complevel=5)
#
ne = [];   nne = []
t = []
_sFiles = glob.glob('dsyn.*.vsh5')
_sFilesInd = np.argsort( np.array( [ int(s[5:-5])  for s in _sFiles ] ) )
sFiles = [_sFiles[i] for i in _sFilesInd]
nt = len(sFiles)
for f in sFiles :
  h5 = tables.open_file(f, 'r')
  nne.append(h5.root.normalizedPerturbation[:,:])
  ne.append(h5.root.densityPerturbation[:,:])
  t.append(h5.get_node_attr(h5.root.time, "vsTime"))
  h5.close()
ne = np.array(ne); nne = np.array(nne)
t = np.array(t)
crossNe = np.zeros(np.shape(ne))
nTime, nR, nZ, nNull = np.shape(ne)
for ir in range(nR) :
  for iz in range(nZ) :
    crossNe[:, ir, iz, 0] = np.correlate(ne[:, ir, iz, 0], ne[:, reference1[0], reference1[1], 0], 'same')
freqCoh, coher = plt.cohere(nne[:,reference1[0],reference1[1],0], nne[:,reference2[0],reference2[1],0], noverlap=0, NFFT=nt, Fs=1e3) # sample freq in kHz)
freqCSD, crossPower = plt.csd(nne[:,reference1[0],reference1[1],0], nne[:,reference2[0],reference2[1],0], noverlap=0, NFFT=nt, Fs=1e3) # sample freq in kHz
for i in range(len(crossPower)) :
    print("%12.6e\t%12.6e\t%12.6e"%(freqCSD[i], np.absolute(crossPower[i]), coher[i]))
it = 0    
for f in sFiles :
  h5 = tables.open_file(f, 'r')
  # print('Generating csyn.' + re.findall(r'\d+', f)[0] + '.vsh5 file')
  h5cc = tables.open_file('csyn.' + re.findall(r'\d+', f)[0] + '.vsh5', 'w', filters=FILTERS)
  # Store time
  h5GroupTime = h5cc.create_group(h5cc.root, "time")
  h5cc.set_node_attr(h5GroupTime, "vsType", "time")
  h5cc.set_node_attr(h5GroupTime, "vsStep", h5.get_node_attr(h5.root.time, "vsStep"))
  h5cc.set_node_attr(h5GroupTime, "vsTime", h5.get_node_attr(h5.root.time, "vsTime"))
  # Store grid
  h5Group = h5cc.create_group(h5cc.root, "globalGridGlobal")
  dset = h5cc.create_array(h5Group, "RZ", h5.root.globalGridGlobal.RZ[:,:,:])
  dset.set_attr("vsKind", "structured")
  dset.set_attr("vsType", "mesh")
  dset.set_attr("vsAxisLabels", "R,Z")
  dset.set_attr("vsNumSpatialDims", 2)
  dset.set_attr("vsLabels", "r,z")
  dset.set_attr("vsNumCells", [len(h5.root.globalGridGlobal.RZ), len(h5.root.globalGridGlobal.RZ[0])])
  dset.set_attr("vsStartCell", [0, 0])
  h5Group = h5cc.create_group(h5cc.root, "globalGridGlobalLimits")
  h5cc.set_node_attr(h5Group, "vsKind", "Cartesian")
  h5cc.set_node_attr(h5Group, "vsType", "limits")
  # Store cross-correlation
  dset = h5cc.create_array(h5cc.root, "crossCorrelation", crossNe[it,:,:,:])
  dset.set_attr("vsType", "variable")
  dset.set_attr("vsNumSpatialDims", 2)
  dset.set_attr("vsLabels", "crossCorrelation")
  dset.set_attr("vsCentering", "none")
  dset.set_attr("vsLimits", "/globalGridGlobalLimits")
  dset.set_attr("vsTimeGroup", "time")
  dset.set_attr("vsMesh", "/globalGridGlobal/RZ")
  h5cc.close()
  h5.close()
  it += 1
  
