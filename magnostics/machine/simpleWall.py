#!/bin/env python

import numpy as np
import os
import yaml
import h5py
import optparse


class simpleWall:
    def __init__(self, confFile):

        # Get data which is in a yaml format
        if os.path.exists(confFile):
            with open(confFile) as filestr:
                swConfig = yaml.load(filestr, Loader=yaml.Loader)
        else:
            raise Exception(confFile + " not found.")
        # There is only one key
        self.confFile = confFile
        self.baseName = os.path.splitext(confFile)[0]
        self.machine = swConfig["machine"]
        self.name = swConfig["name"]
        self.nWall = np.int(swConfig["nWall"])
        nw = self.nWall

        RwList = swConfig["Rwall"].split(",")
        ZwList = swConfig["Zwall"].split(",")

        ## Unfortunately configObj lists don't come in with members as
        # numbers
        Rw = np.array(RwList, np.double)
        Zw = np.array(ZwList, np.double)

        # Ensure that order is CCW AND that zeroth element is at
        # R=Rmax.  This makes algorithms using simpleWall easier.
        # First find where Rmax is located
        for i in range(nw):
            if Rw[i] == Rw.max():
                irmax = i
                iprev = irmax - 1
                if iprev == -1:
                    iprev = nw - 1

        # Figure out mapping of new order
        ioList = range(nw)
        if Zw[irmax] < Zw[iprev]:
            ioList.reverse()
            irmax = nw - irmax - 1
        io = np.array(ioList)
        iorder = np.zeros(nw, np.int)
        iorder[0 : nw - irmax] = io[irmax:nw]
        iorder[nw - irmax : nw] = io[0:irmax]

        if Rw[iorder[0]] != Rw[iorder[nw - 1]] or Zw[iorder[0]] != Zw[iorder[nw - 1]]:
            self.nWall = self.nWall + 1
            makePeriodic = True
        else:
            makePeriodic = False

        self.Rwall = np.zeros(self.nWall, np.double)
        self.Zwall = np.zeros(self.nWall, np.double)
        for i in range(nw):
            self.Rwall[i] = Rw[iorder[i]]
            self.Zwall[i] = Zw[iorder[i]]
        if makePeriodic:
            self.Rwall[nw] = self.Rwall[0]
            self.Zwall[nw] = self.Zwall[0]

    def findIntersectSlope(self, RZc, mc):
        """ Calculate the distance of the point RZc[0:1] 
              with the wall along a slope m.  It returns:
              RZintsct[0:1,0:1] where the first index indicates
              (R,Z) and the second index indicates nearest 
              intersect and further intersect.  It also returns
              iIntrsct[0:1] where the index indicates (near,far) intersect
              index within the Rwall,Zwall arrays.
          """
        nsep = self.nWall
        # Start at an offset because we usually now we want the
        # intersects on outboard midplane
        here = np.int(0.75 * nsep)
        prev = here - 1
        lap = 0
        rc = RZc[0]
        zc = RZc[1]
        bc = zc - mc * rc
        RZintrsct = np.zeros((2, 2), np.double)
        iIntrsct = np.zeros(2, dtype=np.int32)
        nIntersect = 0
        while 1:
            rw1 = self.Rwall[prev]
            zw1 = self.Zwall[prev]
            rw2 = self.Rwall[here]
            zw2 = self.Zwall[here]
            drw = rw2 - rw1
            dzw = zw2 - zw1
            #
            # Find intersection of given line and wall line
            #
            #    Non-vertical lines
            if np.abs(drw) > 1.0e-24:
                mw = dzw / drw  # Slope of wall line
                # Check for parallel lines
                if np.abs(mc - mw) < 1.0e-38:
                    print("Checking parallel lines")
                    # Same line so point exists
                    if zw2 - mc * rw2 + bc < 1.0e-38:
                        rp = rw1 + 0.5 * drw
                        zp = zw1 + 0.5 * dzw
                    else:
                        rp = rc
                        zp = zc  # Just need failure
                else:
                    rp = -(zc - zw2 + mw * rw2 - mc * rc) / (mc - mw)
                    zp = zw2 + mw * (rp - rw2)
            # Vertical lines
            else:
                print("Checking vertical lines")
                rp = rw2
                zp = mc * rp + bc
            print(here, rp, zp, rw1, zw1, rw2, zw2)
            #
            # See if point intersects wall
            if (rw1 - rp) * (rw2 - rp) <= 0 and (zw1 - zp) * (zw2 - zp) <= 0:
                RZintrsct[0, nIntersect] = rp
                RZintrsct[1, nIntersect] = zp
                iIntrsct[nIntersect] = here
                nIntersect = nIntersect + 1
                if nIntersect == 2:
                    break
            here = here + 1
            prev = here - 1
            if here == nsep:
                here = 1
                prev = 0
                lap = lap + 1
            if lap == 3:
                print("Problem in finding intersection in findIntersectSlope")
                break
        return RZintrsct, iIntrsct

    def printStdout(self):
        """ Print the data to standard output."""
        print("nWall ", self.nWall)
        print("Rwall, Zwall ", self.nWall)
        for i in range(self.nWall):
            print(self.Rwall[i], self.Zwall[i])

    def dumpH52D(self, h52dfile=None):
        """ Dump the simple wall data to an h5file. 
              If the file name is not given as an argument, the default
              file name is baseName+"_2d.h5"
          """
        if not h52dfile:
            h52dfile = self.baseName + "_2d.h5"
        h5file = h5py.File(h52dfile, "w")
        h5file.attrs.create("title", "Simple wall description")
        h5file.attrs.create("nWall", self.nWall)
        h5file.attrs.create("confFile", self.confFile)
        h5file.attrs.create("baseName", self.baseName)
        h5file.create_dataset("Rwall", data=self.Rwall, dtype="f")
        h5file.create_dataset("Zwall", data=self.Zwall, dtype="f")
        h5file.close()

    def dumpH53D(self, h53dfile=None, nPhi=20):
        """ Calculate the positions in of a wall in 3D configuration
              space and then dump the data in h5 file format for easy 
              visualization in a program like VISIT. 
              The number of toroidal slices is given as an argument:
              nPhi.  The default is 20 slices.
              If the file name is not given as an argument, the default
              file name is baseName+"_3D.h5"
          """
        if not h53dfile:
            h53dfile = self.baseName + "_3D.h5"
        h5file = h5py.File(h53dfile, "w")
        h5file.attrs.create("title", "Simple wall description")
        h5file.attrs.create("confFile", self.confFile)
        h5file.attrs.create("baseName", self.baseName)
        h5file.attrs.create("confFile", self.confFile)

        # Create a 3D mesh and dump with vsSchema attributes
        wallMesh = np.zeros((self.nWall, nPhi, 1, 3), np.double)
        for i in range(nPhi):
            phi = np.float(i) * 2.0 * np.pi / np.float(nPhi - 1)
            wallMesh[:, i, 0, 0] = self.Rwall[:] * np.cos(phi)
            wallMesh[:, i, 0, 1] = self.Rwall[:] * np.sin(phi)
            wallMesh[:, i, 0, 2] = self.Zwall[:]
        wset = h5file.create_dataset("wallMesh", data=wallMesh)
        wset.attrs.create("vsType", "mesh")
        wset.attrs.create("vsKind", "structured")
        h5file.close()
