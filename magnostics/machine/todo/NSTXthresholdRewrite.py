
from tables import *
from numpy import *
import shutil

#Upper left - working
def popULMask(nZI, r, z, maskData, addon, doprint = False):
    nRI = len(r)-1
    diff = r[nRI] - r[0]
    if z[0] > 0: diff = diff * (-1)
    m = diff / (z[nZI-1] - z[0])
    b = r[0] - m * z[0]
    for j in range(nZI):
        rn = m * z[j] + b
        k = 0
        while r[k] < rn+addon:
            maskData[j, k] = maskData[0, 0]
            k += 1
            if k > nRI-1: break

    return maskData

# Upper right - failing
def popURMask(nZI, r, z, maskData, addon, doprint = False):
    nRI = len(r)-1
    diff = r[0] - r[nRI]
    m = diff / (z[nZI-1] - z[0])
    b = r[nRI] - m * z[0]
    for j in range(nZI):
        rn = m * z[j] + b
        for k in range(nRI+1):
            if r[k] < rn: maskData[j, k] = 1

    return maskData

# Lower left - working
def popLLMask(nZI, r, z, maskData, addon, doprint = False):
    nRI = len(r)-1
    diff = r[0] - r[nRI]
    m = diff / (z[nZI-1] - z[0])
    b = r[nRI] - m * z[0]
    for j in range(nZI):
        rn = m * z[j] + b
        for k in range(nRI, -1, -1):
            if r[k] > rn: maskData[j, k] = 1

    return maskData

# Lower right - failing
def popLRMask(nZI, r, z, maskData, addon, doprint = False):
    nRI = len(r)-1
    diff = r[nRI] - r[0]
    m = diff / (z[nZI-1] - z[0])
    b = r[0] - m * z[0]

    for j in range(nZI):
        rn = m * z[j] + b
        for k in range(nRI, -1, -1):
            if doprint: print "%2d %2d %7.5f %7.5f" % (j, k, r[k], rn)
            if r[k] > rn: maskData[j, k] = 1

    return maskData


addon = 0.001  # A little addon to allow more radii to be caught

threshDir = "/scr2_oxygen/kindig/austin_nstx/threshold/"
threshDir = "./"
threshFile = "complexNSTX_threshold_1.h5"
newthresh = threshDir + "threshold.h5"
maskFile = openFile(threshDir + threshFile, mode = "r")

maskData = maskFile.getNode("/threshold")
md = maskData[:].copy()
od = maskData[:].copy()
r = maskFile.getNode("/globalGridGlobal/r")
z = maskFile.getNode("/globalGridGlobal/z")

shape = md.shape

radius = [True, False]
lastGRI = 0
lastLRI = 0
lastGZI = 0
lastLZI = 0
newLZI = 0
newLRI = 0
newGZI = 0
newGRI = 0
lastbgtz = 0
gtz = where(md[:] > 0.)
md[gtz] = 1
od[gtz] = 1
ftL = True
ftG = True

for i in range(shape[0]):
    gtz = where(md[i, :, 0] >  0.)
    eqz = where(md[i, :, 0] == 0.)
    # Get the number of cells on either side of the 0.5m line
    leng = len(gtz[0])-1
    bgtz = gtz[0][0]
    egtz = gtz[0][leng]
    if i == 272: 
        lastLRI = botLRI
        bgtz = bgtz + 1
        lastGRI = topGRI
        egtz = egtz + 1
    
    # Do the work using linear interpolation between the corners
    doprint = False
    if i == 0:
        lastGRI = egtz
        lastLRI = bgtz
    if egtz != lastGRI:
        # Work on the lower part of the vessel. The slope changes direction
        # past the mid point
        if egtz > lastGRI: 
            rlist = [lastGRI + ind for ind in range(egtz-lastGRI)]
            nr = len(rlist)-1
            md[lastGZI:i, rlist[0]:rlist[nr]+1, 0] = popULMask(i-lastGZI,
                                             r[rlist[0]:rlist[nr]+1], z[lastGZI:i],
                                             md[lastGZI:i, rlist[0]:rlist[nr]+1, 0], addon)
            lastGRI = egtz
            lastGZI = i
        else:
            if ftG:
                topGRI = lastGRI ; botGRI = egtz ; lastGZI = i
                ftG = False
                continue
            if botGRI != egtz:
                rlist = [botGRI + ind for ind in range(topGRI - botGRI)]
                rpass = [r[rl] for rl in rlist]
                zlist = [(lastGZI + ind)-1 for ind in range((i-lastGZI)+1)]
                nr = len(rlist)-1
                nz = len(zlist)-1
                
                md[lastGZI:i, rlist[0]:rlist[nr]+1, 0] = popURMask(i-lastGZI,
                                             r[rlist[0]:rlist[nr]+1], z[lastGZI:i],
                                             md[lastGZI:i, rlist[0]:rlist[nr]+1, 0], addon)
                # Finally set the last indexes
                topGRI = botGRI
                botGRI = egtz
                lastGRI = egtz
                lastGZI = i
        lastGRI = egtz
        lastGZI = i

        # Finally set the last indexes
    if bgtz != lastLRI:
        # Work on the lower part of the vessel. The slope changes direction
        # past the mid point
        # Lower left
        if bgtz < lastLRI:
            rlist = [bgtz    + ind for ind in range(lastLRI-bgtz)]
            rpass = [r[rl] for rl in rlist]
            nextrlist = rlist
            zlist = [lastLZI + ind for ind in range(i-lastLZI)]
            nr = len(rlist)-1
            nz = len(zlist)-1

            md[zlist[0]:zlist[nz]+1, rlist[0]:rlist[nr]+1, 0] = popLLMask(i-lastLZI,
                                                 rpass, z[zlist[0]:zlist[nz]+1],
                                                 md[zlist[0]:zlist[nz]+1, rlist[0]:rlist[nr]+1, 0],
                                                 addon, doprint = doprint)
            # Finally set the last indexes
            lastLRI = bgtz
            lastLZI = i
        else:
        # Lower right - switch to using the right side of the z.
            if ftL:
                botLRI = lastLRI ; topLRI = bgtz ; lastLZI = i
                ftL = False
                continue
            if topLRI != bgtz:
                rlist = [botLRI + ind for ind in range(topLRI - botLRI)]
                rpass = [r[rl] for rl in rlist]
                zlist = [(lastLZI + ind)-1 for ind in range((i-lastLZI)+1)]
                nr = len(rlist)-1
                nz = len(zlist)-1

                md[zlist[0]:zlist[nz]+1, rlist[0]:rlist[nr]+1, 0] = popLRMask(i-lastLZI,
                                                     rpass, z[zlist[0]:zlist[nz]+1],
                                                     md[zlist[0]:zlist[nz]+1, rlist[0]:rlist[nr]+1, 0],
                                                     addon, doprint = doprint)
                # Finally set the last indexes
                botLRI = topLRI
                topLRI = bgtz
                lastLRI = bgtz
                lastLZI = i

md[163, 129:130, 0] = 1
#
# Now write it all out to a new file
print 'Writing to file'

f = openFile(newthresh,mode='a',title="threshold")
f.removeNode("/", "threshold")
f.removeNode("/", "old_threshold")

f.createArray("/", "old_threshold", od, title = "old threshold")
new = f.getNode("/", "old_threshold")
new._f_delAttr("CLASS")
new._f_delAttr("FLAVOR")
new._f_delAttr("VERSION")
new._f_delAttr("TITLE")
new._f_setAttr("time", 0)
new._f_setAttr("vsMesh","globalGridGlobal")
new._f_setAttr("vsType","variable")
new._f_setAttr("vsCentering","edge")
new._f_setAttr("vsTimeGroup","time")
new._f_setAttr("vsLimits","globalGridGlobalLimits")

f.createArray("/", "threshold", md, title = "threshold")
new = f.getNode("/", "threshold")
new._f_delAttr("CLASS")
new._f_delAttr("FLAVOR")
new._f_delAttr("VERSION")
new._f_delAttr("TITLE")
new._f_setAttr("time", 0)
new._f_setAttr("vsMesh","globalGridGlobal")
new._f_setAttr("vsType","variable")
new._f_setAttr("vsCentering","edge")
new._f_setAttr("vsTimeGroup","time")
new._f_setAttr("vsLimits","globalGridGlobalLimits")

f.close()
maskFile.close()
