#!/bin/bash

export PATH=/contrib/hdf5/bin:$PATH
export DIR=/scr2_oxygen/kindig/austin_nstx/visitData

if [[ -d $DIR ]]
then echo $DIR
else exit
fi

echo "Create data files:"
echo
for i in {1..84}
do
   
   export cmd1="python plotStrapSourceMask_nstx.py $i"
#   export cmd2="python plotStrapSource2_nstx.py $i"
   echo $cmd1
   $cmd1
#   echo $cmd2
#   $cmd2
   
   if [[ $i -lt 10 ]]
   then
      export f1=$DIR/combined_components_0$i.vsh5
      export f2=$DIR/data_0$i.h5
      export f3=$DIR/data_00$i.h5
   else
      export f1=$DIR/combined_components_$i.vsh5
      export f2=$DIR/data_$i.h5
      export f3=$DIR/data_0$i.h5
   fi
   
   if ([ -f $f1 ] && [ -f $f2 ])
   then
      export cmd0="h5copy -i $f2 -o $f3 -s solution -d solution"
      export cmd1="h5copy -i $f1 -o $f3 -s threshold -d threshold"
      export cmd2="h5copy -i $f1 -o $f3 -s cartGrid -d cartGrid"
      $cmd0
      echo $cmd0
      $cmd1
      echo $cmd1
      $cmd2
      echo $cmd2
   fi
done

mail kindig@txcorp.com -s "nsxt script done" < threshout.txt
