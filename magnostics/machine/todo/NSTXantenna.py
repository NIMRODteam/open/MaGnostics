#!/bin/env python

from tables import *
from numpy import *
from re import match

# Degrees - There are 12 sections comprising the full radial wall
secAngle = 8.
nantenna =  12.
wallRadius = 1704.9  # Outer wall radius
scale = 1000 # Convert from mm to meters

class Antenna():
    def __init__(self, filename):
        self.rotation = 180.
        self.antfile = openFile(filename)
        for array in self.antfile.walkNodes("/", classname='Array'):
            if array.name == "points": self.points = array
            if array.name == "polygons": self.poly = array

    def findLimits(self):
        minX = min(self.points[:, 0])
        maxX = max(self.points[:, 0])
        minY = min(self.points[:, 1])
        maxY = max(self.points[:, 1])
        minZ = min(self.points[:, 2])
        maxZ = max(self.points[:, 2])

        return (minX, maxX, minY, maxY,minZ, maxZ)

    def findAllAntennas(self):
        """
        The antenna has X values on either side of the the X=0 line, so use the 
        true angle.
        """
        allAntennas = []
        for iA in range(nantenna):
            angle = (secAngle * (iA-(nantenna/2)))-(secAngle/2) + self.rotation

            newpoints = []
            index = 0
            for p in self.points:
                xo = wallRadius - p[2]
                yo = p[0]
                r = sqrt(xo * xo + yo * yo)
                a = angle + 180. * arcsin(p[0] / r) / pi
                xn = r * cos(a * pi / 180.) # p[2]
                yn = r * sin(a * pi / 180.) # p[0]
                zn = p[1] - 476 # Translate down

                # Converting to wall coordinates
                newpoints.append([-1 * xn / scale, yn / scale, zn / scale])

                index += 1
                if index < 2:
                    print angle, r, a, xn, yn, zn, p[0], p[2]
   #                 print p, x, y, z

            allAntennas.append(array(newpoints))

        self.allAntennas = allAntennas

    def writeAntennas(self, filename):
        import os.path, os
        if os.path.exists(filename): os.remove(filename)
        h5 = openFile(filename, "w")
        
        sectionNum = 0
        for ant in self.allAntennas:
            section = "Antenna_%02d" % sectionNum
            sectionNum += 1
            pointsName = "%s_points" % section
            polyName = "%s_poly" % section

            sgroup = h5.createGroup("/", section, 'Poly')
            sgroup._f_setAttr("vsType", "mesh")
            sgroup._f_setAttr("vsKind", "unstructured")
            sgroup._f_setAttr("vsPoints", pointsName)
            sgroup._f_setAttr("vsPolygons", polyName)
            parray = h5.createArray(sgroup, pointsName, ant)
            garray = h5.createArray(sgroup, polyName, self.poly[:])
        h5.close()

    def __del__(self):
        self.antfile.close()

def main():
    import optparse
    parser = optparse.OptionParser(usage="%prog [options] antenna file")
    parser.add_option('-f', '--file', dest='filename',
                      help='Name of the antenna data file - e.g. NSTX_cad_1.h5',
                      default='')
    parser.add_option('-i', '--input', dest='input',
                      help='Name of the antenna data file.',
                      default='')

    options, args = parser.parse_args()

    # Too many arguments
    if len(args) > 2:
      parser.print_usage()
      return
    elif len(args) == 1:
      wallfile=args[0]
    # No arguments
    else:
      if options.filename == '':
        print "Must specify an configuration file"
        return
      else:
        antfile = options.filename

    ant = Antenna(antfile)
    limits = ant.findLimits()

    for v in limits: print v

    ant.findAllAntennas()
    ant.writeAntennas("NSTXantennas_%drot.h5" % ant.rotation)

if __name__ == "__main__": main()
