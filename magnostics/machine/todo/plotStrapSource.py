#!/usr/bin/env python
"""
Author: Travis Austin
Date: 12/15/2009

Purpose: Plotting Vorpal date in R/Z/Phi Coordinates

./runPlotStrapSource.py myfile.h5 [timestep]
"""
import sys
import os
import math
import matplotlib
from matplotlib import pylab as p
import numpy
from numpy.fft import fft
import tables
from matplotlib import ticker
from pylab import *

#
# Declare the number of modes and names
#
nmodes = 5
nameR = "YeeElecFieldR"
nameI = "YeeElecFieldI"
nameC = "combined_components_"
time = sys.argv[1]
outDir = "/home/research/kindig/Source/fusion_machine/"
outFileName = "%sdata_%03d.vsh5" % (outDir, time)
outFileName = "%s%s%d.vsh5" % (outDir, nameC, time)

#
# Get filename for plotting
#
fileModeR = []
fileModeI = []
dataDir = "/scr2_oxygen/austin/Files4Marc/"
nameM = "complexAlcatorCMOD_mode"
for i in range(nmodes):
   #complexAlcatorCMOD_mode0_YeeElecFieldR_1
   fileModeR.append("%s%s%d_%s_%d.h5" % (dataDir, i, nameR, time)
   fileModeI.append("%s%s%d_%s_%d.h5" % (dataDir, i, nameI, time)

# 
# Get handle to variable that is being plotted
#
modeHandleR = []
modeHandleI = []
for i in range(nmodes):
   modeHandleR.append(tables.openFile(fileModeR[i],"r"))
   modeHandleI.append(tables.openFile(fileModeI[i],"r"))

#
# Get Global Grid 
#
ggg = modeHandleR[0].root.globalGridGlobal

#
# Get coordinates values in (R,Z) space
#
z = ggg.z
nz = z.shape[0]

z2 = numpy.zeros((nz))
z2[:] = z[:]

r = ggg.r 
nr = r.shape[0]

r2 = numpy.zeros((nr))
r2[:] = r[:]

#
# Get field values
#
fldR = []
fldI = []
for i in range(nmodes):
  fldR.append(modeHandleR[i].getNode("/"+nameR))
  fldI.append(modeHandleI[i].getNode("/"+nameI))

#
# Declare local field arrays
# 
fieldR_z = []
fieldR_r = []
fieldR_f = []
for i in range(nmodes):
  fieldR_z.append(numpy.zeros((nz,nr)))
  fieldR_r.append(numpy.zeros((nz,nr)))
  fieldR_f.append(numpy.zeros((nz,nr)))

fieldI_z = []
fieldI_r = []
fieldI_f = []
for i in range(nmodes):
  fieldI_z.append(numpy.zeros((nz,nr)))
  fieldI_r.append(numpy.zeros((nz,nr)))
  fieldI_f.append(numpy.zeros((nz,nr)))

#
# Pull out the part of array that we are interested in
#
for i in range(nmodes):
  fieldR_z[i][:,:] = fldR[i][:,:,0]
  fieldR_r[i][:,:] = fldR[i][:,:,1]
  fieldR_f[i][:,:] = fldR[i][:,:,2]

  fieldI_z[i][:,:] = fldI[i][:,:,0]
  fieldI_r[i][:,:] = fldI[i][:,:,1]
  fieldI_f[i][:,:] = fldI[i][:,:,2]

#
# Construct each modes dependence on phi
#
dphi = (2.0*math.pi)/100
phis = numpy.arange(0.0,2.0*math.pi+0.001,dphi)
nphi = phis.shape[0]

#
# Create array for phi component of different modes
# 
pcos = []
psin = []
for i in range(nmodes):
  pcos.append(numpy.cos(1.0*i*(phis)))
  psin.append(numpy.sin(1.0*i*(phis)))

totalMode_z = numpy.zeros((nz,nr,nphi))
totalMode_r = numpy.zeros((nz,nr,nphi))
totalMode_f = numpy.zeros((nz,nr,nphi))

mysoln = numpy.zeros((nr,nphi,nz,3))

for i in range(nphi):
   for j in range(nmodes):
      totalMode_z[:,:,i] = totalMode_z[:,:,i] + (pcos[j][i]*fieldR_z[j][:,:])# - psin[j][i]*fieldI_z[j][:,:])
      totalMode_r[:,:,i] = totalMode_r[:,:,i] + (pcos[j][i]*fieldR_r[j][:,:])# - psin[j][i]*fieldI_r[j][:,:])
      totalMode_f[:,:,i] = totalMode_f[:,:,i] + (pcos[j][i]*fieldR_f[j][:,:])# - psin[j][i]*fieldI_f[j][:,:])

#
# Now Plot the Function in (R,Phi) Space
#

T,R = p.meshgrid(phis,r)

X = R*cos(T)
Y = R*sin(T)

zmid = int(0.5*nz)
maxr = 1.1*max(r)

#figure1 = p.figure()
#p.contourf(X,Y,totalMode_z[zmid,:,:],50)
#p.plot(totalMode_z[zmid,:,0])
#p.axis([-maxr,maxr,-maxr,maxr])
#p.colorbar()

#if sys.argv[2] == "showplot":
#   p.figure(2)
#   p.plot(totalMode_z[zmid,:,0])
#   p.show()
#elif sys.argv[2] == "plot":
#   p.savefig(sys.argv[3])


cartXYZs = numpy.zeros((nr,nphi,nz,3))
for k in range(nz):
   for j in range(nphi):
      for i in range(nr):
         cartXYZs[i,j,k,0] = r[i]*cos(phis[j])
         cartXYZs[i,j,k,1] = r[i]*sin(phis[j])
         cartXYZs[i,j,k,2] = z[k]
         mysoln[i,j,k,0] = totalMode_z[k,i,j]
         mysoln[i,j,k,1] = totalMode_r[k,i,j]
         mysoln[i,j,k,2] = totalMode_f[k,i,j]

f = tables.openFile(outFileName,mode='w',title=outFileName)

root = f.getNode("/")
root._f_delAttr("VERSION")
root._f_delAttr("TITLE")
root._f_delAttr("CLASS")
root._f_delAttr("PYTABLES_FORMAT_VERSION")
root._f_setAttr("Description","(Z,R,Phi) data generated from VORPAL")

#
# Create Cartesian Grid
#
cartGrid = f.createArray("/","cartGrid",cartXYZs,title="test")
cartGrid._f_delAttr("CLASS")
cartGrid._f_delAttr("FLAVOR")
cartGrid._f_delAttr("TITLE")
cartGrid._f_delAttr("VERSION")
cartGrid._f_setAttr("units","m")
cartGrid._f_setAttr("vsIndexOrder","[0,1,2,3]")
cartGrid._f_setAttr("vsKind","structured")
cartGrid._f_setAttr("vsType","mesh")

#
# 
#
gridGroup = f.createGroup(f.root,"vorpalLogicalGrid")
gridGroup._f_delAttr("CLASS")
gridGroup._f_delAttr("TITLE")
gridGroup._f_delAttr("VERSION")
gridGroup._f_setAttr("vsAxis0","zGrid")
gridGroup._f_setAttr("vsAxis1","radialGrid")
gridGroup._f_setAttr("vsAxis2","phiGrid")
gridGroup._f_setAttr("vsKind","rectilinear")
gridGroup._f_setAttr("vsType","mesh")
f.createArray("/vorpalLogicalGrid","z",z2,"m")
f.createArray("/vorpalLogicalGrid","r",r2,"m")
f.createArray("/vorpalLogicalGrid","phi",phis,"radians")

times = numpy.zeros((1))
times[0] = 0.0001
solnGroup = f.createGroup(f.root,"solution")
solnGroup._f_delAttr("VERSION")
solnGroup._f_delAttr("TITLE")
solnGroup._f_delAttr("CLASS")
solnGroup._f_setAttr("step_number","1")
solnGroup._f_setAttr("time",times)
solnGroup._f_setAttr("time_units","s")

f.createArray("/solution","field",mysoln,"V/m")
fieldNode = f.getNode("/solution/field") 
fieldNode._f_delAttr("CLASS")
fieldNode._f_delAttr("FLAVOR")
fieldNode._f_delAttr("VERSION")
fieldNode._f_delAttr("TITLE")
fieldNode._f_setAttr("units","V/m")
fieldNode._f_setAttr("vsMesh","/cartGrid")
fieldNode._f_setAttr("vsType","variable")
f.close()


#
# Close the files that have been opened
#
for i in range(nmodes):
   modeHandleR[i].close()
   modeHandleI[i].close()
