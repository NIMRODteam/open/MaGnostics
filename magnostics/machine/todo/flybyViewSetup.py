class fly1():
    def __init__(self):
        pass
    def getScale(self):
        PScale = [2 for i in range(10)]
        return PScale

    def getNormal(self):
        zNorp =  0.25
        xNorp =  1.00
        yNorp =  1.00
        zNorm = -0.25
        xNorm = -1.00
        yNorm = -1.00
        xyz00 =  0.00
        VNorml = [(-0.5, -.75, 0.25), 
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25),
                  (-0.5, -.75, 0.25)]
        return VNorml

#    def getNormalLeft(self):
#        zNorp =  0.25
#        xNorp =  1.00
#        yNorp =  1.00
#        zNorm = -0.25
#        xNorm = -1.00
#        yNorm = -1.00
#        xyz00 =  0.00
#        VNorml = [( xyz00  , yNorm, zNorp), \
#                  ( xyz00  , yNorm, zNorp), \
#                  ( xNorm/2, yNorm, zNorp), \
#                  ( xNorm, yNorm, zNorp), \
#                  ( xNorm, xyz00, zNorp), \
#                  ( xNorm, yNorp, zNorp), \
#                  ( xNorp, yNorp, zNorp), \
#                  ( xNorm, xyz00, zNorp), \
#                  ( xNorm/2, yNorm, zNorp), \
#                  ( xNorm/2, yNorm, zNorp), \
#                  ( xNorm/2, yNorm, zNorp)]
#
#        return VNorml

    def getClipping(self):
        FarClp = 2.5
#        NearClp = [-2, -1, -0.4, -0.5, -0.5, -0.5, -0.1, -0.1, -1, -2, -2]
        NearClp = [-2 for i in range(10)]
        return NearClp

    def getFocus(self):
        focus = [(0.75, 0, 0) for i in range(10)]
        return focus

#    def getFocusLeft(self):
#        from numpy import * 
#        radip =  0.75
#        radim = -0.75
#        zFocp =  0.00
#        xFocp = radip * cos(45.*pi/180)
#        yFocp = radip * cos(45.*pi/180)
#        zFocm =  0.00
#        xFocm = radim * cos(45.*pi/180)
#        yFocm = radim * cos(45.*pi/180)
#        xyz00 =  0.00
#        focus = [( radim, xyz00, zFocp), \
#                 ( radim, xyz00, zFocp), \
#                 ( radim, xyz00, zFocp), \
#                 ( -0.53,  0.53, zFocp), \
#                 ( xyz00, radip, zFocp), \
#                 (  0.53,  0.53, zFocp), \
#                 ( radip, xyz00, zFocp), \
#                 ( radip, xyz00, zFocp), \
#                 ( radip, xyz00, zFocp), \
#                 ( radip, xyz00, zFocp), \
#                 ( xyz00, xyz00, zFocp)]
#
#        return focus
    
