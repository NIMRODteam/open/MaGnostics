#import sys
#sys.path.append("/home/kindig/software/visit2_2_1.linux-intel/2.2.1/linux-intel/lib")
#from visit import *
#Launch()

from flyby_setup import *

class NSTX():
    def __init__(self):
        self.FinalZoom = 5
        self.Near = -1.5
        self.Far  = 2.5
        self.FinalZoom = 1
        self.FarClp = 2.5

        # Get the setup for the view
        import flybyViewSetup as vSetup
        fvs = vSetup.fly1()
        self.focus = fvs.getFocus()
        self.PScale = fvs.getScale()
        self.VNorml = fvs.getNormal()
        self.NearClp = fvs.getClipping()

        aa = len(self.focus)
        if aa != len(self.focus) or aa != len(self.PScale) or \
           len(self.VNorml) != aa or len(self.NearClp) != aa:
            print aa, len(self.PScale), len(self.VNorml), len(self.NearClp)
#            import sys
            sys.exit("Something is wrong")

        # For Elevate. Yields data = 0.4
        self.magScale = 1000
        # For Elevate. Yields data = 0.8
        self.magScale =  500

        self.viewUp = (0,0,1)
        self.VAngle = 30
        self.axesvisible = 0
        self.perspective = 3
        self.imageZoom = 1
        self.dataColorTable = "orangehot"
#        self.dataColorTable = "hot_desaturated"
        self.wallColorTable = "gray"
        self.calcminmax = 1
        self.mm = (0,  75)   # Roughly 25%, 75%
        self.nLevels = 12
        self.startSec = 0
        self.endSec = 20

#        self.flyDir = './images/'
        self.flyDir = './'

        self.dataDir = "oxygen:/scr2_oxygen/kindig/austin_nstx/visitData/smooth/"
#        self.localDir = "localhost:/home/kindig/projects/fusion_machine/nstx/"
        self.localDir = "oxygen:/home/research/kindig/Source/fusion_machine/nstx/"
#        self.localDir = "/home/research/kindig/projects/fusion_machine/nstx/"
        self.nstxDatabase = self.dataDir + "data_*.h5"
        self.wallFile = self.localDir + "wallUS.h5"
        self.antennaFile = self.localDir + "NSTXantennas_180rot.h5"

    def DeleteExpressions(self):
        for e in Expressions():
            print e
            DeleteExpression(e[0])

    def inAndOut(self, index):
        c = View3DAttributes()
        c.viewNormal = self.VNorml[index]
        c.parallelScale = self.PScale[index]
        c.focus = self.focus[index]
        c.nearPlane = self.NearClp[index]
        c.viewUp = self.viewUp
        c.viewAngle = self.VAngle
        c.farPlane = self.Far
        c.perspective = self.perspective
        c.imageZoom = self.imageZoom
        SetView3D(c)
        
        
    def FlyBy(self, time, iter, N, nsteps):
        cpts = []
        for i in range(len(self.PScale)):
            c = View3DAttributes()
            c.viewNormal = self.VNorml[i]
            c.parallelScale = self.PScale[i]
            c.focus = self.focus[i]
            c.nearPlane = self.NearClp[i]
            c.viewUp = self.viewUp
            c.viewAngle = self.VAngle
            c.farPlane = self.Far
            c.perspective = self.perspective
            c.imageZoom = self.imageZoom
            cpts.append(c)

        cpts = tuple(cpts)

        x=[]
        nc = len(cpts)
        for j in range(nc): x = x + [float(j) / (float(nc)-1)]
        t = float(time) / float(nsteps - 1)
        c = EvalCubicSpline(t, x, cpts)
        SetView3D(c)
        vA = open("viewAtt.txt", "a")
        vA.write("%5d: %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f\n" % \
          (time, c.viewNormal[0], c.viewNormal[1], c.viewNormal[2], \
           c.focus[0], c.focus[1], c.focus[2], c.nearPlane, c.parallelScale))
        vA.close()


    # Create a tuple of camera values and x values. The x values
    # determine where in [0,1] the control points occur.

    def loopTimes(self, name, i, ntimes, nsteps, minimage, timeMultiplier, save, b):
        time = i % ntimes
        time = time
        TimeSliderSetState(time)
        b.text = "Time: %4d" % time
        DrawPlots()
        pL = GetPlotList()
        legend = GetAnnotationObject(pL.GetPlots(8).plotName)
        legend.textColor = (255, 255, 255, 255)
        legend.useForegroundForTextColor = 0
        flyby = False
        if flyby:
            if i > minimage: self.FlyBy(i, i - (minimage + 1), ntimes, nsteps)
            else: self.FlyBy(i, timeMultiplier, ntimes, nsteps)
        else:
            self.inAndOut(time)

        if save:
            sw = SaveWindowAttributes()
            sw.fileName = "%s%s%03d" % (self.flyDir, name, i)
            sw.family = 0
            sw.width = 1024
            sw.height =  768
            sw.screenCapture = 1
            sw.resConstraint = sw.NoConstraint
            SetSaveWindowAttributes(sw)
            SaveWindow()

    def conductFlyBy(self, timeMultiplier = 4):
        ntimes = TimeSliderGetNStates()
        ntimes = 84
        nsteps = ntimes * timeMultiplier

        name = 'FlyByTest'
        name = "SliceMagnitudeYElevBright"
        minimage = ntimes
        b = CreateAnnotationObject("TimeSlider")

        if 0 == 1:
            for i in range(nsteps):
                self.loopTimes(name, i, ntimes, nsteps, minimage, timeMultiplier, True, b)
        else:
            ii = [i +   0 for i in range(1)]
            for i in ii:
                self.loopTimes(name, i, ntimes, nsteps, minimage, timeMultiplier, True, b)

    def createFieldMagnitude(self, nstxDatabase):
        print self.nstxDatabase
        result = OpenDatabase(self.nstxDatabase + " database", 0)
        if result == 0: prns
        self.magName = "Magnitude"
        expr = "sqrt(sqr(<solution/field_0>) + \
                     sqr(<solution/field_1>) + \
                     sqr(<solution/field_2>))"
        print expr
        DefineScalarExpression(self.magName, expr)
        return self.magName

    def scaleMagnitude(self, scale):
        self.scaledMagName = "scaledMagName"
        expr = "<%s> / %d" % (self.magName, scale)
        DefineScalarExpression(self.scaledMagName, expr)
        return self.scaledMagName
        
    def __del__(self):
        self.DeleteExpressions()
nstx = NSTX()

# Create the image to fly by
DeleteAllPlots()
nstx.DeleteExpressions()
#mm = getNSTXDataMinMax( name )
# Add the magnitude and test to see that the data is available
name = nstx.createFieldMagnitude( nstx.nstxDatabase )
scaledMagName = nstx.scaleMagnitude(nstx.magScale)

# Add the vessel
getNSTXWall(nstx, nstx.wallFile, start = nstx.startSec, end = nstx.endSec )
addAntenna( nstx, nstx.antennaFile )

#Add the data
#getNSTXDataContour( nstx, name, nstx.mm[0], nstx.mm[1], True )
getNSTXDataPC( nstx, name, nstx.mm[0], nstx.mm[1], 1, 1.00)
getThreshold(name)
getSlice("Y", p2d = 1)
getElevate(scaledMagName)
getTransform()
getNSTXDataPC( nstx, name, nstx.mm[0], nstx.mm[1], 0, 1.0)
getThreshold(name)
getSlice("Z", p2d = 0)
#getElevate(scaledMagName)

setAnnotation( nstx )
setLighting( nstx )
setRenderingAttributes()
#setSaveWindowAtts()

viewAtt = open("viewAtt.txt", "w")
viewAtt.write("%6s %7s %7s %7s %7s %7s %7s %7s %7s\n" % \
  ("Time", "NormX", "NormY", "NormZ", "FocusX", "FocusY", "FocusZ", "nearClp", "Scale"))
viewAtt.close()


drawplots = False
mult = 6
if drawplots:
    att3d = View3DAttributes()
    att3d.viewNormal = (-0.25, -1, .25)
    att3d.viewUp = (0, 0, 1)
    att3d.focus = (0.75, 0, 0)
    att3d.parallelScale = 2
    att3d.nearPlane = -6
    att3d.farPlane = 6
    SetView3D(att3d)
    TimeSliderGetNStates()
    TimeSliderSetState(65)
    DrawPlots()
else: nstx.conductFlyBy(timeMultiplier = mult)
