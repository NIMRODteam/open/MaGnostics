#!/bin/env python

from tables import *
from numpy import *
from re import match

# Degrees - There are 12 sections comprising the full radial wall
scale = 1
secAngle = 7.5

class Wall():
    def __init__(self, filename):
        wallfile = open(filename)
        wall = []

        # Read the file
        for l in wallfile:
            line = l.split(",")
            new = []
            for i in line:
                if not match("[A-Za-z\s]", i) or len(i) == 0:
                    new.append(float(i.strip()))
                else:
                    new.append(i.strip())
            wall.append(new)

        self.header = wall.pop(0)
        self.wallRZHW = wall
        print self.header

    def calcWallRZ(self):
        def upperLeft(posRZ):
            radians = posRZ[4] * pi / 180.
            ulR = posRZ[0] - posRZ[2]/2. * cos(radians) - posRZ[3]/2. * sin(radians)
            ulZ = posRZ[1] - posRZ[2]/2. * sin(radians) + posRZ[3]/2. * cos(radians)
            return (ulR * 1, ulZ * 1)

        def upperRight(posRZ):
            radians = posRZ[4] * pi / 180.
            ulR = posRZ[0] + posRZ[2]/2. * cos(radians) - posRZ[3]/2. * sin(radians)
            ulZ = posRZ[1] + posRZ[2]/2. * sin(radians) + posRZ[3]/2. * cos(radians)
            return (ulR * 1, ulZ * 1)

        def lowerRight(posRZ):
            radians = posRZ[4] * pi / 180.
            ulR = posRZ[0] + posRZ[2]/2. * cos(radians) + posRZ[3]/2. * sin(radians)
            ulZ = posRZ[1] + posRZ[2]/2. * sin(radians) - posRZ[3]/2. * cos(radians)
            return (ulR * 1, ulZ * 1)

        def lowerLeft(posRZ):
            radians = posRZ[4] * pi / 180.
            ulR = posRZ[0] - posRZ[2]/2. * cos(radians) + posRZ[3]/2. * sin(radians)
            ulZ = posRZ[1] - posRZ[2]/2. * sin(radians) - posRZ[3]/2. * cos(radians)
            return (ulR * 1, ulZ * 1)

        # Calculate the corner of each wall item as a thin slab
        wallXZ = []
        index = 0
        itemlist = [21, 22, 23, 27, 34, 38, 39, 40, 60, 61, 62, 63, 64, 65, 66, 67]
        for line in self.wallRZHW:
            if index == 2:
                line[4] = 60
            ul = upperLeft(line)
            ur = upperRight(line)
            lr = lowerRight(line)
            ll = lowerLeft(line)
            section = [ur, ul, ll, lr]  # Vertical D-Shape
            wallXZ.append(section)
            if index >= 1 and index <= 3:
                print "%02d %7.4f, %7.2f, %7.2f, %7.2f, %9.2f, %10.2f" % \
                     (index,line[0], line[1], line[2], line[3], line[4], line[5])
                print " %9.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f" % \
                    (section[0][0], section[0][1], section[1][0], section[1][1], \
                     section[2][0], section[2][1], section[3][0], section[3][1])
            index += 1

        self.wallXZ = wallXZ

    # Calculate the XYZ for each corner and populate a 2, 2, 2 mesh for each
    # point.
    def calc3dWall(self):
        wall = []
        shape = (2, 2, 2, 3)
        nwall = 360/secAngle
        if (360/secAngle) * secAngle != 360: print nwall
        else: nwall = int(nwall)
        for a in range(nwall):
            angle = a * secAngle
            ang1 = angle - (secAngle / 2.)
            ang2 = angle + (secAngle / 2.)
            section = []
            for item in self.wallXZ:
                box = []
                for corner in item:
                    c1 = self.calcEnd(ang1, corner)
                    c2 = self.calcEnd(ang2, corner)
                    box.append(c1)
                    box.append(c2)
                tmpBox = array(box)
                newBox = reshape(tmpBox, shape)
                section.append(array(newBox))
            wall.append(section)
        wallarr = array(wall)
        self.wall = wallarr

    def calcEnd(self, angle, corner):
        radius = self.calcOuterRadius(corner[0])
        x = radius * cos(angle * pi / 180.)
        y = radius * sin(angle * pi / 180.)
        z = corner[1]

        return (x, y, z)    # D-Shape
#        return (y+7, z+476, -1*x+1704.9)   # lazy D-Shape
                                               

    def calcOuterRadius(self, x):
         """
         Since the wall is in 12 30 deg sections, there is an inner radius
         given by the R of the NSTX wall file. But each section is flat,
         so the outer radius is found by using the first point at X, Y using
         X = R at an angle of 15 deg. This is done for the inner and outer X
         for each item in the wall.
         """
         angle = secAngle / 2.
         radius = x/cos(angle * pi /180.)
         return radius

    def makeSectionUnstructMesh(self):
        """
        Convert the 2x2x2x3 mesh for each compontent (item) into an unstructured
        mesh for each section.
        """
        self.sectpnts = []
        self.sectpoly = []
        for section in self.wall:
            points  = []   # Point list
            polygon = []   # Polygon list
            index = 0
            for item in section:
                # Put each vertex in the points list
                points.append(item[0][0][0])  # 0
                points.append(item[0][0][1])  # 1
                points.append(item[0][1][1])  # 2
                points.append(item[0][1][0])  # 3

                points.append(item[1][1][1])  # 4
                points.append(item[1][1][0])  # 5
                points.append(item[1][0][0])  # 6
                points.append(item[1][0][1])  # 7

                # Create a polygon list using the two following maps
                # (Nverts, vertex1, vertex2, vertex3, vertex4)
                i0 = index
                i1 = index + 1
                i2 = index + 2
                i3 = index + 3
                i4 = index + 4
                i5 = index + 5
                i6 = index + 6
                i7 = index + 7

                polygon.append((4, i0, i1, i2, i3))
                polygon.append((4, i0, i1, i4, i5))
                polygon.append((4, i0, i3, i6, i5))
                polygon.append((4, i3, i2, i7, i6))
                polygon.append((4, i2, i7, i4, i1))
                polygon.append((4, i4, i5, i6, i7))
                index += 8

            self.sectpnts.append(points)
            self.sectpoly.append(polygon)

    def writeUS3D(self, filename, mode = 'w', title = ""):
        
        h5 = openFile(filename, mode = mode, title = title)
        sectionNum = 0
        for points, poly in zip(self.sectpnts, self.sectpoly):
            p = array(points)
            g = array(poly)
            section = "WallSection_%02d" % sectionNum
            pointsname = '%s_Points' % section
            polyname = '%s_Polygons' % section
            sectionNum += 1
            sgroup = h5.createGroup("/", section, 'Poly')
            sgroup._f_setAttr("vsType", "mesh")
            sgroup._f_setAttr("vsKind", "unstructured")
            sgroup._f_setAttr("vsPoints", pointsname)
            sgroup._f_setAttr("vsPolygons", polyname)
            parray = h5.createArray(sgroup, pointsname, p)
            garray = h5.createArray(sgroup, polyname, g)
        h5.close()

    def write2D(self, filename, mode = 'w', title = ""):
        h5 = openFile(filename, mode = mode, title = title)
        itemNum = 0 
        shape = (2, 2, 2)
        for item in self.wallXZ:
            newitem = reshape(array(item), shape)
            name = 'Wall_I%02d' % (itemNum)
            item = h5.createArray('/', name, newitem)
            item.attrs.vsType = "mesh"
            item.attrs.vsKind = "structured"
            item.attrs.vsIndexOrder = "compMinorC"
            itemNum += 1
        h5.close()
        

    def writeH5(self, filename, mode = "w", title = ""):
        h5 = openFile(filename, mode = mode, title = title)
        sectionNum = 0
        for section in self.wall:
            itemNum = 0 
            for item in section:
                name = 'Wall_S%02d_I%02d' % (sectionNum, itemNum)
                item = h5.createArray('/', name, item)
                item.attrs.vsType = "mesh"
                item.attrs.vsKind = "structured"
                item.attrs.vsIndexOrder = "compMinorC"
                itemNum += 1
            sectionNum += 1
        h5.close()


#############
### MAIN ####
#############
def main():
    import optparse
    parser = optparse.OptionParser(usage="%prog [options] Wall CSV file")
    parser.add_option('-f', '--file', dest='filename',
                      help='Name of simple Wall CSV file if not specified as argument.',
                      default='')
    parser.add_option('-i', '--input', dest='input',
                      help='Name of simple Wall configuration file if not specified as argument.',
                      default='')

    options, args = parser.parse_args()

    # Too many arguments
    if len(args) > 2:
      parser.print_usage()
      return
    elif len(args) == 1:
      wallfile=args[0]
    # No arguments
    else:
      if options.filename == '':
        print "Must specify an configuration file"
        return
      else:
        wallfile = options.filename

    wallRZHW = Wall(wallfile)
    wallRZHW.calcWallRZ()
    wallRZHW.calc3dWall()
    wallRZHW.makeSectionUnstructMesh()
    wallRZHW.writeH5('wall.h5', title = 'NSTX Wall')
    wallRZHW.write2D('wall2d.h5', title = '2D NSTX Wall')
    wallRZHW.writeUS3D('wallUS.h5', title = "Unstructured Mesh for each Section")

    print wallRZHW.wall.shape

#    for item in wallRZHW.wall:
#        for section in item: print section


if __name__ == "__main__": main()
