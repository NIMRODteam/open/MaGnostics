#!/usr/bin/env python

import os
import sys

# This is to handle python3 changes to how import paths work
# This is to allow calling this file directly, otherwise I would
# use the import .PSF syntax
machine_dir = os.path.dirname(__file__)
sys.path.append(machine_dir)

import numpy
import matplotlib.pyplot as plt
import tables
import optparse
from simpleWall import simpleWall


class simpleWallPlot(simpleWall):
    def plot(self, noShow=False):
        fig = plt.plot(self.Rwall, self.Zwall, linestyle="-", color="red")
        plt.axis("equal")
        plt.xlabel("R (m)")
        plt.ylabel("Z (m)")
        wallTitle = "Simple wall from " + self.baseName
        plt.title(wallTitle)
        if not noShow:
            plt.show()
        return fig


def main():
    parser = optparse.OptionParser(usage="%prog [options] configFile")
    parser.add_option(
        "-i",
        "--input",
        dest="input",
        help="Name of simple Wall configuration file if not specified as argument.",
        default="",
    )
    parser.add_option(
        "-p",
        "--print",
        dest="doPrint",
        help="Print all possible fields to standard out along with description",
        action="store_true",
    )
    parser.add_option(
        "-l",
        "--plot",
        dest="doPlot",
        help="Make 2D plot of the simple wall",
        action="store_true",
    )
    parser.add_option(
        "-a",
        "--all",
        dest="doAll",
        action="store_true",
        help="Write out all hdf5 output files (using default names if not specified by other options).",
        default="",
    )
    parser.add_option(
        "--h52D",
        dest="h52Dname",
        default="",
        help="Name of 2D output file (.h5 format).",
    )
    parser.add_option(
        "--h53D", dest="h53Dname", default="", help="Name of HDF5 3D file to write data"
    )
    parser.add_option(
        "-n",
        "--nphi",
        dest="nphi",
        default="20",
        help="Number of toroidal phi planes to use in writing HDF5 3D file",
    )

    options, args = parser.parse_args()

    # Too many arguments
    if len(args) > 1:
        parser.print_usage()
        return
    elif len(args) == 1:
        configFile = args[0]
    # No arguments
    else:
        if options.input == "":
            parser.print_usage()
            return
        else:
            configFile = options.input

    sw = simpleWallPlot(configFile)

    # If output not written, then just write to stdout:
    if options.doPrint:
        sw.printStdout()

    printH52D = False
    printH53D = False
    if options.doAll:
        printH52D = True
        printH53D = True

    nPhiIn = numpy.int(options.nphi)

    if options.h52Dname:
        sw.dumpH52D(h52dfile=options.h52Dname)
    elif printH52D:
        sw.dumpH52D()
    if options.h53Dname:
        sw.dumpH53D(h53dfile=options.h53Dname, nPhi=nPhiIn)
    elif printH53D:
        sw.dumpH53D(nPhi=nPhiIn)

    if options.doPlot:
        sw.plot()


if __name__ == "__main__":
    main()
