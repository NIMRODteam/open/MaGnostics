shotfile=../../data/test/psfShotFile.yml
geomfile=../../data/test/psfGeometry.yml
./PSFchannelsPlot.py -d ../../data/test/psf_read1 -s $shotfile $geomfile -D
mv psfChannels.h5 psfChannels_read1.h5
./PSFchannelsPlot.py -d ../../data/test/psf_read2 -s $shotfile $geomfile -D
mv psfChannels.h5 psfChannels_read2.h5
./PSFchannelsPlot.py -d ../../data/test/psf_read3 -s $shotfile $geomfile -D
mv psfChannels.h5 psfChannels_read3.h5
./PSFchannelsPlot.py -s ../../data/test/psf_read4/shotData.yml $geomfile -D
mv psfChannels.h5 psfChannels_read4.h5
