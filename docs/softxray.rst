
Getting started running the soft X-ray synthetic diagnostic
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The basic steps of doing a soft X-ray synthetic diagnostic are:

      1. Specify the lines for each soft X-ray channel in each array (or
         collection)
      2. Determine the function that acts as the kernel for the line integration
         (must get from diagnostician as it depends on filters and stuff)
      3. Evalate the density and temperature fields from your code over each 
         line grid and calculate

.. todolist::

      1.  Put in equation for PSF integration
      2.  Add in the references
      3.  Add in the images for each command



