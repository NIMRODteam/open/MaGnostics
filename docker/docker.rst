Developer Tools
===============

Run MaGnostics development environment in a Docker container
-------------------------------------------------------------

To start a container from the MaGnostics pre-built Docker image on GitLab, run:

.. code:: bash

    [host]$ cd docker
    [host]$ echo COMPOSE_PROJECT_NAME=$USER > .env  # [optional] specify a project name
    [host]$ docker-compose pull  # pull the most up-to-date version of the MaGnostics base image
    [host]$ docker-compose up -d  # start the container

This will mount the local MaGnostics source directory into the container.
We recommend you use a ".env" file to specify an alternate project name (the default 
being "magnostics").  This will let you run multiple isolated environments on a 
single host.  

If you want to build a new Docker image using the Dockerfile, run: 

.. code:: bash

    [host]$ cd docker
    [host]$ echo COMPOSE_PROJECT_NAME=$USER > .env  # [optional] specify a project name
    [host]$ docker-compose up --build -d  # build the container using Dockerfile and start it


Then to launch an interactive Bash session inside that container, do:

.. code:: bash

    [host]$ docker-compose exec magnostics bash

By default, the container comes with:
    1. python3
    2. pip3
    3. git
    4. vim

If you need to install any other packeges on your local Docker image, 
you can use "apt" or "pip3":

.. code:: bash

    [container]$ apt install package_name
    [container]$ pip3 install package_name

Do not forget to cleanup after yourself:

.. code:: bash

    [container]$ exit
    [host]$ docker-compose down # stop and remove the container
