#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_simplewallplot
----------------------------------

Tests for magnostics.machine.simplewallplot module.
"""

import os
import numpy as np
import unittest
import magnostics.machine.simpleWallPlot as swplot


class TestSimpleWallPlot(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        parent_dir = os.path.dirname(os.path.dirname(__file__))
        data_dir = os.path.join(parent_dir, "magnostics", "data")
        confFile = os.path.join(data_dir, "diii-d", "diiidSimpleWall.yml")
        cls.myswplot = swplot.simpleWallPlot(confFile)

    @classmethod
    def tearDownClass(cls):
        del cls.myswplot

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_simplewallplot(self):
        """
        Doesn't test the plot directly but whether matplotlib
        datastructures are being loaded correctly
        """
        fig = self.myswplot.plot(noShow=True)
        r = self.myswplot.Rwall
        z = self.myswplot.Zwall
        r_plot = fig[0].get_data()[0]
        z_plot = fig[0].get_data()[1]
        np.testing.assert_array_equal(r, r_plot)
        np.testing.assert_array_equal(z, z_plot)
        return
