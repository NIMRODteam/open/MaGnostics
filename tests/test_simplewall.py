#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_simplewall
----------------------------------

Tests for magnostics.machine.simpleWall module.
"""
import os
import unittest
import magnostics.machine.simpleWall as simplewall


class TestPsf(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        parent_dir = os.path.dirname(os.path.dirname(__file__))
        data_dir = os.path.join(parent_dir, "magnostics", "data")
        confFile = os.path.join(data_dir, "diii-d", "diiidSimpleWall.yml")
        cls.mywall = simplewall.simpleWall(confFile)

    @classmethod
    def tearDownClass(cls):
        del cls.mywall

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_simplewall_diiid(self):
        self.assertEqual(self.mywall.name, "diiidSimpleWall")
        self.assertEqual(self.mywall.machine, "diii-d")
        # these are not what a naive reading of yaml file will
        # show because of fixes to the ordering and periodicity
        self.assertEqual(self.mywall.nWall, 90)
        self.assertAlmostEqual(self.mywall.Rwall[0], 2.377)
        self.assertAlmostEqual(self.mywall.Zwall[0], -0.389)

    def test_simplewall_diiid_h5(self):
        self.mywall.dumpH52D()
        self.mywall.dumpH53D()
