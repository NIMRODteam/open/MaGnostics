#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_rzpoints
----------------------------------

Tests for magnostics.diagnostics.rzpoints module.
"""
import os
import unittest
import magnostics.diagnostics.PSF.rzpoints as rzp
import yaml


class TestPsf(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        parent_dir = os.path.dirname(os.path.dirname(__file__))
        data_dir = os.path.join(parent_dir, "magnostics", "data")
        confFile = os.path.join(data_dir, "test", "rzpoints.yml")
        with open(confFile) as filestr:
            inputConf = yaml.load(filestr, Loader=yaml.Loader)

        cls.mypoints = rzp.diagnosticPoints(inputConf["array1"])

    @classmethod
    def tearDownClass(cls):
        del cls.mypoints

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_rzpoints(self):
        self.assertAlmostEqual(self.mypoints.RZpts[0, 3, 3], 1.96)
        self.assertAlmostEqual(self.mypoints.RZpts[1, 3, 3], 0.06)
