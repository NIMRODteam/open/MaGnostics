#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_rzpointsplot
----------------------------------

Tests for magnostics.diagnostics.rzpointsplot module.
"""
import os
import unittest
import magnostics.diagnostics.PSF.rzpoints_plot as rzp
import yaml


class TestDiagnosticPointsPlot(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        parent_dir = os.path.dirname(os.path.dirname(__file__))
        data_dir = os.path.join(parent_dir, "magnostics", "data")
        confFile = os.path.join(data_dir, "test", "rzpoints.yml")
        with open(confFile) as filestr:
            inputConf = yaml.load(filestr, Loader=yaml.Loader)

        cls.mypoints = rzp.pointsPlot(inputConf["array1"])

    @classmethod
    def tearDownClass(cls):
        del cls.mypoints

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_rzpointsplot(self):
        """
        Doesn't test the plot directly but whether matplotlib
        datastructures are being loaded correctly
        """
        fig = self.mypoints.plot(noShow=True)
        r = self.mypoints.RZpts[0, 0, 0]
        z = self.mypoints.RZpts[1, 0, 0]
        # get_offset used with scatterplot
        r_plot = fig.get_offsets()[0, 0]
        z_plot = fig.get_offsets()[0, 1]  # Indices reversed
        self.assertAlmostEqual(r, r_plot)
        self.assertAlmostEqual(z, z_plot)
        return
