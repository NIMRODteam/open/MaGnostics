#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_psf
----------------------------------

Tests for magnostics.diagnostics.PSF module.
"""

import numpy as np
import unittest
import magnostics.diagnostics.PSF.psf_plot as psfplot


class TestPsfPlot(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.mypsf = psfplot.PSFplot()

    @classmethod
    def tearDownClass(cls):
        del cls.mypsf

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_psfplot(self):
        """
        Doesn't test the plot directly but whether matplotlib
        datastructures are being loaded correctly
        """
        # I can't figure out how to get the data
        # Datastructures for matplotlib/mplot are confusing
        return
        fig = self.mypsf.plotSurface(noShow=True)
        # print(fig.get_axes())
        print(dir(fig))
        r = self.mypsf.grid[0]
        z = self.mypsf.grid[1]
        # r_plot, z_plot = fig.get_axes().get_xydata().T
        line = fig.gca()
        r_plot, z_plot, f_plot = line._verts3d
        np.testing.assert_array_equal(r, r_plot)
        np.testing.assert_array_equal(z, z_plot)
        return
