#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_psfchannels
--------

Tests for magnostics.diagnostics.PSF module.
"""

import os
import unittest
import magnostics.diagnostics.PSF.PSFchannels as psfc

test_dir = os.path.dirname(__file__)
data_dir = os.path.join(os.path.dirname(test_dir), "magnostics", "data")
confFile = os.path.join(data_dir, "test", "psfConfig.yml")


class TestPsf(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.mypc = psfc.psfChannelArrays(configFile=confFile)

    @classmethod
    def tearDownClass(cls):
        del cls.mypc

    def setUp(self):
        pass

    def tearDown(self):
        pass

    # Same as test_rzpoints.py test because array specification is same
    def test_rzpoints(self):
        self.assertAlmostEqual(self.mypc.array1.RZpts[0, 3, 3], 1.96)
        self.assertAlmostEqual(self.mypc.array1.RZpts[1, 3, 3], 0.06)

    def test_info(self):
        self.assertEqual(int(self.mypc.info["shot"]), 123456)

    def test_arrayNames(self):
        self.assertEqual(self.mypc.arrayNames[0], "array1")

    def test_addPSF(self):
        psfFile = os.path.join(data_dir, "test", "psf.yml")
        self.mypc.addPSF(psfFile=psfFile)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[0][0, 0],
                               1.5985)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[1][0, 0],
                               -0.0015)

    def test_psf_read1(self):
        self.mypc = psfc.psfChannelArrays(configFile=confFile)
        psfDir = os.path.join(data_dir, "test", "psf_read1")
        self.mypc.addPSF(psfDir=psfDir)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[0][0, 0],
                               1.5985)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[1][0, 0],
                               -0.0015)

    def test_psf_read2(self):
        self.mypc = psfc.psfChannelArrays(configFile=confFile)
        psfDir = os.path.join(data_dir, "test", "psf_read2")
        self.mypc.addPSF(psfDir=psfDir)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[0][0, 0],
                               1.9729999)
        self.assertAlmostEqual(
            self.mypc.array1.psfArray[3, 3].grid[1][0, 0],
            -0.024999999
        )

    def test_psf_read3(self):
        self.mypc = psfc.psfChannelArrays(configFile=confFile)
        psfDir = os.path.join(data_dir, "test", "psf_read3")
        self.mypc.addPSF(psfDir=psfDir)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[0][0, 0],
                               1.9729999)
        self.assertAlmostEqual(
            self.mypc.array1.psfArray[3, 3].grid[1][0, 0], -0.024999999
        )

    def test_psf_read4(self):
        psfDir = os.path.join(data_dir, "test", "psf_read4")
        newConfFile = os.path.join(psfDir, "psfConfig.yml")
        self.mypc = psfc.psfChannelArrays(configFile=newConfFile)
        self.assertEqual(self.mypc.hasPsfInfo, True)
        self.mypc.addPSF()
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[0][0, 0],
                               1.9729999)
        self.assertAlmostEqual(
            self.mypc.array1.psfArray[3, 3].grid[1][0, 0], -0.024999999
        )

    def test_psf_read5(self):
        confFileMin = os.path.join(data_dir, "test", "psfConfigMin.yml")
        self.mypc = psfc.psfChannelArrays(configFile=confFileMin)
        psfDir = os.path.join(data_dir, "test", "psf_read5")
        self.mypc.addPSF(psfDir=psfDir, Rshift=10.0, Zshift=-1.0)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[0][0, 0],
                               11.6255)
        self.assertAlmostEqual(self.mypc.array1.psfArray[3, 3].grid[1][0, 0],
                               -1.0075)
        values = self.mypc.array1.psfArray[3, 3].values
        intgrl = self.mypc.array1.psfArray[3, 3].integrate(values)
        self.assertAlmostEqual(intgrl, 4.581922320949428e-6)

    def test_dump(self):
        self.mypc.dump(h5FileName="psfc_test.h5")

    def test_restore(self):
        # Module function
        psfc.restore(h5FileName="psfc_test.h5")
